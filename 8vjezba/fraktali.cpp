//g++ -o konveksan_poligon konveksan_poligon.cpp -lglut -lGL
//odmah pritisnuti c
//upisati naziv datoteke
//upisati koordinate tocke
//unijeti skaliranje

#include <stdio.h>
#include <iostream>
#include<GL/glut.h>
#include <fstream>
#include <bits/stdc++.h>
#include <boost/algorithm/string.hpp>

using namespace std;

GLuint window;
GLuint width = 1080, height = 1080;
int eps;
int m;
float umin, umax, vmin, vmax;
float creal;
float cimag;

void myDisplay();
void myReshape(int width, int height);
void myKeyboard(unsigned char theKey, int mouseX, int mouseY);

int main(int argc, char ** argv)
{
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	glutInitWindowSize(width, height);
	glutInitWindowPosition(100, 100);
	glutInit(&argc, argv);

	window = glutCreateWindow("Fraktali");
	glutReshapeFunc(myReshape);
	glutDisplayFunc(myDisplay);
    glutKeyboardFunc(myKeyboard);
    printf("Pritisni m za crtanje Mandelbrota ili j za crtanje Julije\n");
		
	glutMainLoop();
	return 0;
}

void myDisplay()
{
	glClear(GL_COLOR_BUFFER_BIT);
	glFlush();
}

void myReshape(int w, int h)
{
	//printf("Pozvan myReshape()\n");
	width = w; height = h;
	glViewport(0, 0, width, height);
	
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0, width - 1, height - 1, 0, 0, 1);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glClearColor(1.0f, 1.0f, 1.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT);
	glPointSize(1.0);
	glColor3f(0.0f, 0.0f, 0.0f);
}

void myKeyboard(unsigned char theKey, int mouseX, int mouseY)
{
    if (theKey == 'm') {
        printf("Ucitajte epsilon i maksimalan broj iteracija: ");
        cin >> eps >> m;
        printf("Ucitajte umin, umax, vmin i vmax: ");
        cin >> umin >> umax >> vmin >> vmax;

        for (int i = 1; i <= height; i++) {
            for (int j = 1; j <= width; j++) {
                float u0 = (umax - umin)*j/width + umin;
                float v0 = (vmax - vmin)*i/height + vmin;
                int k = -1;
                float creal = u0;
                float cimag = v0;
                complex<double> z(0, 0); 
                float r = 0;
                while (r < eps && k < m) {
                    k ++;
                    complex<double> c(creal, cimag); 
                    z = pow(z, 2) + c;
                    r = sqrt(pow(real(z), 2) + pow(imag(z), 2));
                }
                glBegin(GL_POINTS);
                    if (k == m) glColor3f(0, 0, 0);
                    else glColor3f(min(1.0 - (double)k/m, (double)k/m), 0, (double)k/m);
                    glVertex2i(j, i);
                glEnd();
            }
        }
    } else if (theKey == 'j') {
        printf("Ucitajte epsilon i maksimalan broj iteracija: ");
        cin >> eps >> m;
        printf("Ucitajte umin, umax, vmin, vmax, creal i cimag: ");
        cin >> umin >> umax >> vmin >> vmax >> creal >> cimag;

        for (int i = 1; i <= height; i++) {
            for (int j = 1; j <= width; j++) {
                float u0 = (umax - umin)*j/width + umin;
                float v0 = (vmax - vmin)*i/height + vmin;
                int k = -1;
                complex<double> z(u0, v0); 
                float r = 0;
                while (r < eps && k < m) {
                    k ++;
                    complex<double> c(creal, cimag); 
                    z = pow(z, 2) + c;
                    r = sqrt(pow(real(z), 2) + pow(imag(z), 2));
                }
                glBegin(GL_POINTS);
                    if (k == m) glColor3f(0, 0, 0);
                    else glColor3f(min(1.0 - (double)k/m, (double)k/m), 0, (double)k/m);
                    glVertex2i(j, i);
                glEnd();
            }
        }
    }
	glFlush();
}