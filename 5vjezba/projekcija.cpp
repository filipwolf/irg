//g++ -o konveksan_poligon konveksan_poligon.cpp -lglut -lGL
//odmah pritisnuti c
//upisati naziv datoteke
//upisati koordinate tocke
//unijeti skaliranje

#include <stdio.h>
#include <iostream>
#include <GL/glut.h>
#include <fstream>
#include <bits/stdc++.h>
#include <boost/algorithm/string.hpp>
#include <glm/glm.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include "glm/ext.hpp"

using namespace std;

GLint n;
GLdouble Lx[1000], Ly[1000];
GLdouble a[1000], b[1000], c[1000];
GLint Ix;
float Vx, Vy, Vz;
bool bigFlag = false;

GLuint window;
GLuint width = 1000, height = 800;
GLint ymin, ymax, xmin, xmax;

vector<tuple<float, float, float>> vertices;
vector<tuple<float, float, float>> vertices2;
vector<tuple<float, float, float>> vertices3;
vector<tuple<int, int, int>> polygons;
tuple<int, int, int> glediste;
tuple<int, int, int> ociste;

void myDisplay();
void myReshape(int width, int height);
void myKeyboard(unsigned char theKey, int mouseX, int mouseY);

int main(int argc, char ** argv)
{
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	glutInitWindowSize(width, height);
	glutInitWindowPosition(100, 100);
	glutInit(&argc, argv);

	window = glutCreateWindow("Glut OpenGL tijelo");
	glutReshapeFunc(myReshape);
	glutDisplayFunc(myDisplay);
    glutKeyboardFunc(myKeyboard);
    printf("Pritisni \"C\" za crtanje\n");
		
	glutMainLoop();
	return 0;
}

void myDisplay()
{
	glClear(GL_COLOR_BUFFER_BIT);
	glFlush();
}

void myReshape(int w, int h)
{
	//printf("Pozvan myReshape()\n");
	width = w; height = h;
	Ix = 0;
	glViewport(0, 0, width, height);
	
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0, width - 1, height - 1, 0, 0, 1);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glClearColor(1.0f, 1.0f, 1.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT);
	glPointSize(1.0);
	glColor3f(0.0f, 0.0f, 0.0f);
}

void myKeyboard(unsigned char theKey, int mouseX, int mouseY)
{
    ifstream object;
    string line;

    int vertexCnt = 0, polygonCnt = 0;
	float xmax, xmin, ymax, ymin, zmax, zmin;
	float max;
	float middlex, middley, middlez;
	float scale;
	string fileName;
    string koords;
	if (theKey == 'C') {
		vertices.clear();
		vertices2.clear();
		vertices3.clear();
		polygons.clear();
		printf("Zadajte datoteku objekta: ");
		cin >> fileName;
		object.open(fileName);
		while ( getline (object,line) ) {
			if (line[0] == 'v') {
				size_t pos = 0;
				string token;
				string delimiter = " ";
				vector<float> v;
				while ((pos = line.find(delimiter)) != string::npos) {
					token = line.substr(0, pos);
					if (token.compare("v") == 0) {
						line.erase(0, pos + delimiter.length());
						continue;
					}
					v.push_back(stof(token));
					line.erase(0, pos + delimiter.length());
				}
				vertices.push_back(make_tuple(v[0], v[1], stof(line)));
				vertexCnt++;
			}
			if (line[0] == 'f') {
				size_t pos = 0;
				string token;
				string delimiter = " ";
				vector<int> v;
				while ((pos = line.find(delimiter)) != string::npos) {
					token = line.substr(0, pos);
					if (token.compare("f") == 0) {
						line.erase(0, pos + delimiter.length());
						continue;
					}
					v.push_back(stoi(token));
					line.erase(0, pos + delimiter.length());
				}
				polygons.push_back(make_tuple(v[0], v[1], stoi(line)));
				polygonCnt++;
			}
		}
		object.close();

		printf("Zadajte datoteku koordinata: ");
		cin >> koords;
		object.open(koords);
		getline (object,line);
		size_t pos = 0;
		string token;
		string delimiter = " ";
		vector<float> v;
		while ((pos = line.find(delimiter)) != string::npos) {
			token = line.substr(0, pos);
			if (token.compare("v") == 0) {
				line.erase(0, pos + delimiter.length());
				continue;
			}
			v.push_back(stof(token));
			line.erase(0, pos + delimiter.length());
		}
		glediste = make_tuple(v[0], v[1], v[2]);
		ociste = make_tuple(v[3], v[4], stoi(line));
		//cout << get<0>(glediste) << get<1>(glediste) << get<2>(glediste) << endl;
		//cout << get<0>(ociste) << get<1>(ociste) << get<2>(ociste) << endl;
		object.close();
		xmax = get<0>(vertices[0]);
		xmin = get<0>(vertices[0]);
		ymax = get<1>(vertices[0]);
		ymin = get<1>(vertices[0]);
		zmax = get<2>(vertices[0]);
		zmin = get<2>(vertices[0]);
		for ( const auto& i : vertices ) {
			//cout << get<0>(i) << get<1>(i) << get<2>(i) << endl;
			if (get<0>(i) > xmax) xmax = get<0>(i);
			if (get<0>(i) < xmin) xmin = get<0>(i);
			if (get<1>(i) > ymax) ymax = get<1>(i);
			if (get<1>(i) < ymin) ymin = get<1>(i);
			if (get<2>(i) > zmax) zmax = get<2>(i);
			if (get<2>(i) < zmin) zmin = get<2>(i);
		}
		//cout << xmax << xmin << ymax << ymin << zmax << zmin << endl;
		middlex = (xmax + xmin) / 2.0;
		middley = (ymax + ymin) / 2.0;
		middlez = (zmax + zmin) / 2.0;
		if (xmax > ymax) max = xmax;
		else max = ymax;
		if (zmax > max) max = zmax;
		//cout << middlex << middley << middlez << "\n";
		for ( const auto& i : vertices ) {
			//cout << get<0>(i) << get<1>(i) << get<2>(i) << endl;
			vertices2.push_back(make_tuple(get<0>(i) - middlex, get<1>(i) - middley, get<2>(i) - middlez));
		}
		xmax = get<0>(vertices2[0]);
		xmin = get<0>(vertices2[0]);
		ymax = get<1>(vertices2[0]);
		ymin = get<1>(vertices2[0]);
		zmax = get<2>(vertices2[0]);
		zmin = get<2>(vertices2[0]);
		for ( const auto& i : vertices2 ) {
			//cout << get<0>(i) << get<1>(i) << get<2>(i) << endl;
			if (get<0>(i) > xmax) xmax = get<0>(i);
			if (get<0>(i) < xmin) xmin = get<0>(i);
			if (get<1>(i) > ymax) ymax = get<1>(i);
			if (get<1>(i) < ymin) ymin = get<1>(i);
			if (get<2>(i) > zmax) zmax = get<2>(i);
			if (get<2>(i) < zmin) zmin = get<2>(i);
		}
		//cout << xmax << xmin << ymax << ymin << zmax << zmin << endl;
		middlex = (xmax + xmin) / 2.0;
		middley = (ymax + ymin) / 2.0;
		middlez = (zmax + zmin) / 2.0;
		//cout << middlex << middley << middlez << "\n";
		if (xmax > ymax) max = xmax;
		else max = ymax;
		if (zmax > max) max = zmax;
		for ( const auto& i : vertices2 ) {
			//cout << get<0>(i)/max << get<1>(i)/max << get<2>(i)/max << endl;
			vertices3.push_back(make_tuple(get<0>(i)/max, get<1>(i)/max, get<2>(i)/max));
		}
		glFlush();
	} 
	else if (theKey == 'k') {
		get<0>(ociste) ++;
		glm::mat4 T1;
		T1[0] = glm::vec4(1, 0, 0, - get<0>(ociste));
		T1[1] = glm::vec4(0, 1, 0, - get<1>(ociste));
		T1[2] = glm::vec4(0, 0, 1, - get<2>(ociste));
		T1[3] = glm::vec4(0, 0, 0, 1);
		GLfloat xg1 = get<0>(glediste) - get<0>(ociste);
		GLfloat yg1 = get<1>(glediste) - get<1>(ociste);
		GLfloat zg1 = get<2>(glediste) - get<2>(ociste);
		glm::mat4 T2;
		GLfloat sina = yg1/sqrt(pow(xg1, 2) + pow(yg1, 2));
		GLfloat cosa = xg1/sqrt(pow(xg1, 2) + pow(yg1, 2));
		T2[0] = glm::vec4(cosa, sina, 0, 0);
		T2[1] = glm::vec4(- sina, cosa, 0, 0);
		T2[2] = glm::vec4(0, 0, 1, 0);
		T2[3] = glm::vec4(0, 0, 0, 1);
		GLfloat xg2 = sqrt(pow(xg1,2) + pow(yg1, 2));
		GLfloat yg2 = 0;
		GLfloat zg2 = zg1;
		GLfloat sinb = xg2/sqrt(pow(xg2, 2) + pow(zg2, 2));
		GLfloat cosb = zg2/sqrt(pow(xg2, 2) + pow(zg2, 2));
		glm::mat4 T3;
		T3[0] = glm::vec4(cosb, 0, - sinb, 0);
		T3[1] = glm::vec4(0, 1, 0, 0);
		T3[2] = glm::vec4(sinb, 0, cosb, 0);
		T3[3] = glm::vec4(0, 0, 0, 1);
		GLfloat xg3 = 0;
		GLfloat yg3 = 0;
		GLfloat zg3 = sqrt(pow(xg2, 2) + pow(zg2, 2));
		glm::mat4 T4;
		T4[0] = glm::vec4(0, 1, 0, 0);
		T4[1] = glm::vec4(- 1, 0, 0, 0);
		T4[2] = glm::vec4(0, 0, 1, 0);
		T4[3] = glm::vec4(0, 0, 0, 1);
		glm::mat4 T5;
		T5[0] = glm::vec4(- 1, 0, 0, 0);
		T5[1] = glm::vec4(0, 1, 0, 0);
		T5[2] = glm::vec4(0, 0, 1, 0);
		T5[3] = glm::vec4(0, 0, 0, 1);
		glm::mat4 T1_5 = T1 * T2 * T3 * T4 * T5;

		
		scale = 200;
		for ( const auto& i : polygons ) {
			glColor3f(0,0,0);
			glBegin(GL_TRIANGLES);
				//cout << scale*get<0>(vertices3[get<0>(i)]) << scale*get<1>(vertices3[get<0>(i)]) << "\n";
				glm::vec4 A0 = glm::vec4(get<0>(vertices3[get<0>(i) - 1]), get<1>(vertices3[get<0>(i) - 1]), get<2>(vertices3[get<0>(i) - 1]), 1);
				glm::vec4 As = A0 * T1_5;
				GLfloat H = sqrt(pow(As[0] - get<0>(glediste), 2) + pow(As[1] - get<1>(glediste), 2) + pow(As[2] - get<2>(glediste), 2));
				GLfloat xp = (As[0] / As[2]) * H;
				GLfloat yp = (As[1] / As[2]) * H;
				glVertex2f(scale*xp + scale, scale*yp + scale);
				A0 = glm::vec4(get<0>(vertices3[get<1>(i) - 1]), get<1>(vertices3[get<1>(i) - 1]), get<2>(vertices3[get<1>(i) - 1]), 1);
				As = A0 * T1_5;
				H = sqrt(pow(As[0] - get<0>(glediste), 2) + pow(As[1] - get<1>(glediste), 2) + pow(As[2] - get<2>(glediste), 2));
				xp = (As[0] / As[2]) * H;
				yp = (As[1] / As[2]) * H;
				glVertex2f(scale*xp + scale, scale*yp + scale);
				A0 = glm::vec4(get<0>(vertices3[get<2>(i) - 1]), get<1>(vertices3[get<2>(i) - 1]), get<2>(vertices3[get<2>(i) - 1]), 1);
				As = A0 * T1_5;
				H = sqrt(pow(As[0] - get<0>(glediste), 2) + pow(As[1] - get<1>(glediste), 2) + pow(As[2] - get<2>(glediste), 2));
				xp = (As[0] / As[2]) * H;
				yp = (As[1] / As[2]) * H;
				glVertex2f(scale*xp + scale, scale*yp + scale);
			glEnd();
		}
		glFlush();
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	} 
	else if (theKey == 'K') {
		get<0>(ociste) --;
		glm::mat4 T1;
		T1[0] = glm::vec4(1, 0, 0, - get<0>(ociste));
		T1[1] = glm::vec4(0, 1, 0, - get<1>(ociste));
		T1[2] = glm::vec4(0, 0, 1, - get<2>(ociste));
		T1[3] = glm::vec4(0, 0, 0, 1);
		GLfloat xg1 = get<0>(glediste) - get<0>(ociste);
		GLfloat yg1 = get<1>(glediste) - get<1>(ociste);
		GLfloat zg1 = get<2>(glediste) - get<2>(ociste);
		glm::mat4 T2;
		GLfloat sina = yg1/sqrt(pow(xg1, 2) + pow(yg1, 2));
		GLfloat cosa = xg1/sqrt(pow(xg1, 2) + pow(yg1, 2));
		T2[0] = glm::vec4(cosa, sina, 0, 0);
		T2[1] = glm::vec4(- sina, cosa, 0, 0);
		T2[2] = glm::vec4(0, 0, 1, 0);
		T2[3] = glm::vec4(0, 0, 0, 1);
		GLfloat xg2 = sqrt(pow(xg1,2) + pow(yg1, 2));
		GLfloat yg2 = 0;
		GLfloat zg2 = zg1;
		GLfloat sinb = xg2/sqrt(pow(xg2, 2) + pow(zg2, 2));
		GLfloat cosb = zg2/sqrt(pow(xg2, 2) + pow(zg2, 2));
		glm::mat4 T3;
		T3[0] = glm::vec4(cosb, 0, - sinb, 0);
		T3[1] = glm::vec4(0, 1, 0, 0);
		T3[2] = glm::vec4(sinb, 0, cosb, 0);
		T3[3] = glm::vec4(0, 0, 0, 1);
		GLfloat xg3 = 0;
		GLfloat yg3 = 0;
		GLfloat zg3 = sqrt(pow(xg2, 2) + pow(zg2, 2));
		glm::mat4 T4;
		T4[0] = glm::vec4(0, 1, 0, 0);
		T4[1] = glm::vec4(- 1, 0, 0, 0);
		T4[2] = glm::vec4(0, 0, 1, 0);
		T4[3] = glm::vec4(0, 0, 0, 1);
		glm::mat4 T5;
		T5[0] = glm::vec4(- 1, 0, 0, 0);
		T5[1] = glm::vec4(0, 1, 0, 0);
		T5[2] = glm::vec4(0, 0, 1, 0);
		T5[3] = glm::vec4(0, 0, 0, 1);
		glm::mat4 T1_5 = T1 * T2 * T3 * T4 * T5;

		
		scale = 200;
		for ( const auto& i : polygons ) {
			glColor3f(0,0,0);
			glBegin(GL_TRIANGLES);
				//cout << scale*get<0>(vertices3[get<0>(i)]) << scale*get<1>(vertices3[get<0>(i)]) << "\n";
				glm::vec4 A0 = glm::vec4(get<0>(vertices3[get<0>(i) - 1]), get<1>(vertices3[get<0>(i) - 1]), get<2>(vertices3[get<0>(i) - 1]), 1);
				glm::vec4 As = A0 * T1_5;
				GLfloat H = sqrt(pow(As[0] - get<0>(glediste), 2) + pow(As[1] - get<1>(glediste), 2) + pow(As[2] - get<2>(glediste), 2));
				GLfloat xp = (As[0] / As[2]) * H;
				GLfloat yp = (As[1] / As[2]) * H;
				glVertex2f(scale*xp + scale, scale*yp + scale);
				A0 = glm::vec4(get<0>(vertices3[get<1>(i) - 1]), get<1>(vertices3[get<1>(i) - 1]), get<2>(vertices3[get<1>(i) - 1]), 1);
				As = A0 * T1_5;
				H = sqrt(pow(As[0] - get<0>(glediste), 2) + pow(As[1] - get<1>(glediste), 2) + pow(As[2] - get<2>(glediste), 2));
				xp = (As[0] / As[2]) * H;
				yp = (As[1] / As[2]) * H;
				glVertex2f(scale*xp + scale, scale*yp + scale);
				A0 = glm::vec4(get<0>(vertices3[get<2>(i) - 1]), get<1>(vertices3[get<2>(i) - 1]), get<2>(vertices3[get<2>(i) - 1]), 1);
				As = A0 * T1_5;
				H = sqrt(pow(As[0] - get<0>(glediste), 2) + pow(As[1] - get<1>(glediste), 2) + pow(As[2] - get<2>(glediste), 2));
				xp = (As[0] / As[2]) * H;
				yp = (As[1] / As[2]) * H;
				glVertex2f(scale*xp + scale, scale*yp + scale);
			glEnd();
		}
		glFlush();
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	} 
	else if (theKey == 'l') {
		get<1>(ociste) ++;
		glm::mat4 T1;
		T1[0] = glm::vec4(1, 0, 0, - get<0>(ociste));
		T1[1] = glm::vec4(0, 1, 0, - get<1>(ociste));
		T1[2] = glm::vec4(0, 0, 1, - get<2>(ociste));
		T1[3] = glm::vec4(0, 0, 0, 1);
		GLfloat xg1 = get<0>(glediste) - get<0>(ociste);
		GLfloat yg1 = get<1>(glediste) - get<1>(ociste);
		GLfloat zg1 = get<2>(glediste) - get<2>(ociste);
		glm::mat4 T2;
		GLfloat sina = yg1/sqrt(pow(xg1, 2) + pow(yg1, 2));
		GLfloat cosa = xg1/sqrt(pow(xg1, 2) + pow(yg1, 2));
		T2[0] = glm::vec4(cosa, sina, 0, 0);
		T2[1] = glm::vec4(- sina, cosa, 0, 0);
		T2[2] = glm::vec4(0, 0, 1, 0);
		T2[3] = glm::vec4(0, 0, 0, 1);
		GLfloat xg2 = sqrt(pow(xg1,2) + pow(yg1, 2));
		GLfloat yg2 = 0;
		GLfloat zg2 = zg1;
		GLfloat sinb = xg2/sqrt(pow(xg2, 2) + pow(zg2, 2));
		GLfloat cosb = zg2/sqrt(pow(xg2, 2) + pow(zg2, 2));
		glm::mat4 T3;
		T3[0] = glm::vec4(cosb, 0, - sinb, 0);
		T3[1] = glm::vec4(0, 1, 0, 0);
		T3[2] = glm::vec4(sinb, 0, cosb, 0);
		T3[3] = glm::vec4(0, 0, 0, 1);
		GLfloat xg3 = 0;
		GLfloat yg3 = 0;
		GLfloat zg3 = sqrt(pow(xg2, 2) + pow(zg2, 2));
		glm::mat4 T4;
		T4[0] = glm::vec4(0, 1, 0, 0);
		T4[1] = glm::vec4(- 1, 0, 0, 0);
		T4[2] = glm::vec4(0, 0, 1, 0);
		T4[3] = glm::vec4(0, 0, 0, 1);
		glm::mat4 T5;
		T5[0] = glm::vec4(- 1, 0, 0, 0);
		T5[1] = glm::vec4(0, 1, 0, 0);
		T5[2] = glm::vec4(0, 0, 1, 0);
		T5[3] = glm::vec4(0, 0, 0, 1);
		glm::mat4 T1_5 = T1 * T2 * T3 * T4 * T5;

		
		scale = 200;
		for ( const auto& i : polygons ) {
			glColor3f(0,0,0);
			glBegin(GL_TRIANGLES);
				//cout << scale*get<0>(vertices3[get<0>(i)]) << scale*get<1>(vertices3[get<0>(i)]) << "\n";
				glm::vec4 A0 = glm::vec4(get<0>(vertices3[get<0>(i) - 1]), get<1>(vertices3[get<0>(i) - 1]), get<2>(vertices3[get<0>(i) - 1]), 1);
				glm::vec4 As = A0 * T1_5;
				GLfloat H = sqrt(pow(As[0] - get<0>(glediste), 2) + pow(As[1] - get<1>(glediste), 2) + pow(As[2] - get<2>(glediste), 2));
				GLfloat xp = (As[0] / As[2]) * H;
				GLfloat yp = (As[1] / As[2]) * H;
				glVertex2f(scale*xp + scale, scale*yp + scale);
				A0 = glm::vec4(get<0>(vertices3[get<1>(i) - 1]), get<1>(vertices3[get<1>(i) - 1]), get<2>(vertices3[get<1>(i) - 1]), 1);
				As = A0 * T1_5;
				H = sqrt(pow(As[0] - get<0>(glediste), 2) + pow(As[1] - get<1>(glediste), 2) + pow(As[2] - get<2>(glediste), 2));
				xp = (As[0] / As[2]) * H;
				yp = (As[1] / As[2]) * H;
				glVertex2f(scale*xp + scale, scale*yp + scale);
				A0 = glm::vec4(get<0>(vertices3[get<2>(i) - 1]), get<1>(vertices3[get<2>(i) - 1]), get<2>(vertices3[get<2>(i) - 1]), 1);
				As = A0 * T1_5;
				H = sqrt(pow(As[0] - get<0>(glediste), 2) + pow(As[1] - get<1>(glediste), 2) + pow(As[2] - get<2>(glediste), 2));
				xp = (As[0] / As[2]) * H;
				yp = (As[1] / As[2]) * H;
				glVertex2f(scale*xp + scale, scale*yp + scale);
			glEnd();
		}
		glFlush();
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	} 
	else if (theKey == 'L') {
		get<1>(ociste) --;
		glm::mat4 T1;
		T1[0] = glm::vec4(1, 0, 0, - get<0>(ociste));
		T1[1] = glm::vec4(0, 1, 0, - get<1>(ociste));
		T1[2] = glm::vec4(0, 0, 1, - get<2>(ociste));
		T1[3] = glm::vec4(0, 0, 0, 1);
		GLfloat xg1 = get<0>(glediste) - get<0>(ociste);
		GLfloat yg1 = get<1>(glediste) - get<1>(ociste);
		GLfloat zg1 = get<2>(glediste) - get<2>(ociste);
		glm::mat4 T2;
		GLfloat sina = yg1/sqrt(pow(xg1, 2) + pow(yg1, 2));
		GLfloat cosa = xg1/sqrt(pow(xg1, 2) + pow(yg1, 2));
		T2[0] = glm::vec4(cosa, sina, 0, 0);
		T2[1] = glm::vec4(- sina, cosa, 0, 0);
		T2[2] = glm::vec4(0, 0, 1, 0);
		T2[3] = glm::vec4(0, 0, 0, 1);
		GLfloat xg2 = sqrt(pow(xg1,2) + pow(yg1, 2));
		GLfloat yg2 = 0;
		GLfloat zg2 = zg1;
		GLfloat sinb = xg2/sqrt(pow(xg2, 2) + pow(zg2, 2));
		GLfloat cosb = zg2/sqrt(pow(xg2, 2) + pow(zg2, 2));
		glm::mat4 T3;
		T3[0] = glm::vec4(cosb, 0, - sinb, 0);
		T3[1] = glm::vec4(0, 1, 0, 0);
		T3[2] = glm::vec4(sinb, 0, cosb, 0);
		T3[3] = glm::vec4(0, 0, 0, 1);
		GLfloat xg3 = 0;
		GLfloat yg3 = 0;
		GLfloat zg3 = sqrt(pow(xg2, 2) + pow(zg2, 2));
		glm::mat4 T4;
		T4[0] = glm::vec4(0, 1, 0, 0);
		T4[1] = glm::vec4(- 1, 0, 0, 0);
		T4[2] = glm::vec4(0, 0, 1, 0);
		T4[3] = glm::vec4(0, 0, 0, 1);
		glm::mat4 T5;
		T5[0] = glm::vec4(- 1, 0, 0, 0);
		T5[1] = glm::vec4(0, 1, 0, 0);
		T5[2] = glm::vec4(0, 0, 1, 0);
		T5[3] = glm::vec4(0, 0, 0, 1);
		glm::mat4 T1_5 = T1 * T2 * T3 * T4 * T5;

		
		scale = 200;
		for ( const auto& i : polygons ) {
			glColor3f(0,0,0);
			glBegin(GL_TRIANGLES);
				//cout << scale*get<0>(vertices3[get<0>(i)]) << scale*get<1>(vertices3[get<0>(i)]) << "\n";
				glm::vec4 A0 = glm::vec4(get<0>(vertices3[get<0>(i) - 1]), get<1>(vertices3[get<0>(i) - 1]), get<2>(vertices3[get<0>(i) - 1]), 1);
				glm::vec4 As = A0 * T1_5;
				GLfloat H = sqrt(pow(As[0] - get<0>(glediste), 2) + pow(As[1] - get<1>(glediste), 2) + pow(As[2] - get<2>(glediste), 2));
				GLfloat xp = (As[0] / As[2]) * H;
				GLfloat yp = (As[1] / As[2]) * H;
				glVertex2f(scale*xp + scale, scale*yp + scale);
				A0 = glm::vec4(get<0>(vertices3[get<1>(i) - 1]), get<1>(vertices3[get<1>(i) - 1]), get<2>(vertices3[get<1>(i) - 1]), 1);
				As = A0 * T1_5;
				H = sqrt(pow(As[0] - get<0>(glediste), 2) + pow(As[1] - get<1>(glediste), 2) + pow(As[2] - get<2>(glediste), 2));
				xp = (As[0] / As[2]) * H;
				yp = (As[1] / As[2]) * H;
				glVertex2f(scale*xp + scale, scale*yp + scale);
				A0 = glm::vec4(get<0>(vertices3[get<2>(i) - 1]), get<1>(vertices3[get<2>(i) - 1]), get<2>(vertices3[get<2>(i) - 1]), 1);
				As = A0 * T1_5;
				H = sqrt(pow(As[0] - get<0>(glediste), 2) + pow(As[1] - get<1>(glediste), 2) + pow(As[2] - get<2>(glediste), 2));
				xp = (As[0] / As[2]) * H;
				yp = (As[1] / As[2]) * H;
				glVertex2f(scale*xp + scale, scale*yp + scale);
			glEnd();
		}
		glFlush();
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	} 
	else if (theKey == 'j') {
		get<2>(ociste) ++;
		glm::mat4 T1;
		T1[0] = glm::vec4(1, 0, 0, - get<0>(ociste));
		T1[1] = glm::vec4(0, 1, 0, - get<1>(ociste));
		T1[2] = glm::vec4(0, 0, 1, - get<2>(ociste));
		T1[3] = glm::vec4(0, 0, 0, 1);
		GLfloat xg1 = get<0>(glediste) - get<0>(ociste);
		GLfloat yg1 = get<1>(glediste) - get<1>(ociste);
		GLfloat zg1 = get<2>(glediste) - get<2>(ociste);
		glm::mat4 T2;
		GLfloat sina = yg1/sqrt(pow(xg1, 2) + pow(yg1, 2));
		GLfloat cosa = xg1/sqrt(pow(xg1, 2) + pow(yg1, 2));
		T2[0] = glm::vec4(cosa, sina, 0, 0);
		T2[1] = glm::vec4(- sina, cosa, 0, 0);
		T2[2] = glm::vec4(0, 0, 1, 0);
		T2[3] = glm::vec4(0, 0, 0, 1);
		GLfloat xg2 = sqrt(pow(xg1,2) + pow(yg1, 2));
		GLfloat yg2 = 0;
		GLfloat zg2 = zg1;
		GLfloat sinb = xg2/sqrt(pow(xg2, 2) + pow(zg2, 2));
		GLfloat cosb = zg2/sqrt(pow(xg2, 2) + pow(zg2, 2));
		glm::mat4 T3;
		T3[0] = glm::vec4(cosb, 0, - sinb, 0);
		T3[1] = glm::vec4(0, 1, 0, 0);
		T3[2] = glm::vec4(sinb, 0, cosb, 0);
		T3[3] = glm::vec4(0, 0, 0, 1);
		GLfloat xg3 = 0;
		GLfloat yg3 = 0;
		GLfloat zg3 = sqrt(pow(xg2, 2) + pow(zg2, 2));
		glm::mat4 T4;
		T4[0] = glm::vec4(0, 1, 0, 0);
		T4[1] = glm::vec4(- 1, 0, 0, 0);
		T4[2] = glm::vec4(0, 0, 1, 0);
		T4[3] = glm::vec4(0, 0, 0, 1);
		glm::mat4 T5;
		T5[0] = glm::vec4(- 1, 0, 0, 0);
		T5[1] = glm::vec4(0, 1, 0, 0);
		T5[2] = glm::vec4(0, 0, 1, 0);
		T5[3] = glm::vec4(0, 0, 0, 1);
		glm::mat4 T1_5 = T1 * T2 * T3 * T4 * T5;

		
		scale = 200;
		for ( const auto& i : polygons ) {
			glColor3f(0,0,0);
			glBegin(GL_TRIANGLES);
				//cout << scale*get<0>(vertices3[get<0>(i)]) << scale*get<1>(vertices3[get<0>(i)]) << "\n";
				glm::vec4 A0 = glm::vec4(get<0>(vertices3[get<0>(i) - 1]), get<1>(vertices3[get<0>(i) - 1]), get<2>(vertices3[get<0>(i) - 1]), 1);
				glm::vec4 As = A0 * T1_5;
				GLfloat H = sqrt(pow(As[0] - get<0>(glediste), 2) + pow(As[1] - get<1>(glediste), 2) + pow(As[2] - get<2>(glediste), 2));
				GLfloat xp = (As[0] / As[2]) * H;
				GLfloat yp = (As[1] / As[2]) * H;
				glVertex2f(scale*xp + scale, scale*yp + scale);
				A0 = glm::vec4(get<0>(vertices3[get<1>(i) - 1]), get<1>(vertices3[get<1>(i) - 1]), get<2>(vertices3[get<1>(i) - 1]), 1);
				As = A0 * T1_5;
				H = sqrt(pow(As[0] - get<0>(glediste), 2) + pow(As[1] - get<1>(glediste), 2) + pow(As[2] - get<2>(glediste), 2));
				xp = (As[0] / As[2]) * H;
				yp = (As[1] / As[2]) * H;
				glVertex2f(scale*xp + scale, scale*yp + scale);
				A0 = glm::vec4(get<0>(vertices3[get<2>(i) - 1]), get<1>(vertices3[get<2>(i) - 1]), get<2>(vertices3[get<2>(i) - 1]), 1);
				As = A0 * T1_5;
				H = sqrt(pow(As[0] - get<0>(glediste), 2) + pow(As[1] - get<1>(glediste), 2) + pow(As[2] - get<2>(glediste), 2));
				xp = (As[0] / As[2]) * H;
				yp = (As[1] / As[2]) * H;
				glVertex2f(scale*xp + scale, scale*yp + scale);
			glEnd();
		}
		glFlush();
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	} 
	else if (theKey == 'J') {
		get<2>(ociste) --;
		glm::mat4 T1;
		T1[0] = glm::vec4(1, 0, 0, - get<0>(ociste));
		T1[1] = glm::vec4(0, 1, 0, - get<1>(ociste));
		T1[2] = glm::vec4(0, 0, 1, - get<2>(ociste));
		T1[3] = glm::vec4(0, 0, 0, 1);
		GLfloat xg1 = get<0>(glediste) - get<0>(ociste);
		GLfloat yg1 = get<1>(glediste) - get<1>(ociste);
		GLfloat zg1 = get<2>(glediste) - get<2>(ociste);
		glm::mat4 T2;
		GLfloat sina = yg1/sqrt(pow(xg1, 2) + pow(yg1, 2));
		GLfloat cosa = xg1/sqrt(pow(xg1, 2) + pow(yg1, 2));
		T2[0] = glm::vec4(cosa, sina, 0, 0);
		T2[1] = glm::vec4(- sina, cosa, 0, 0);
		T2[2] = glm::vec4(0, 0, 1, 0);
		T2[3] = glm::vec4(0, 0, 0, 1);
		GLfloat xg2 = sqrt(pow(xg1,2) + pow(yg1, 2));
		GLfloat yg2 = 0;
		GLfloat zg2 = zg1;
		GLfloat sinb = xg2/sqrt(pow(xg2, 2) + pow(zg2, 2));
		GLfloat cosb = zg2/sqrt(pow(xg2, 2) + pow(zg2, 2));
		glm::mat4 T3;
		T3[0] = glm::vec4(cosb, 0, - sinb, 0);
		T3[1] = glm::vec4(0, 1, 0, 0);
		T3[2] = glm::vec4(sinb, 0, cosb, 0);
		T3[3] = glm::vec4(0, 0, 0, 1);
		GLfloat xg3 = 0;
		GLfloat yg3 = 0;
		GLfloat zg3 = sqrt(pow(xg2, 2) + pow(zg2, 2));
		glm::mat4 T4;
		T4[0] = glm::vec4(0, 1, 0, 0);
		T4[1] = glm::vec4(- 1, 0, 0, 0);
		T4[2] = glm::vec4(0, 0, 1, 0);
		T4[3] = glm::vec4(0, 0, 0, 1);
		glm::mat4 T5;
		T5[0] = glm::vec4(- 1, 0, 0, 0);
		T5[1] = glm::vec4(0, 1, 0, 0);
		T5[2] = glm::vec4(0, 0, 1, 0);
		T5[3] = glm::vec4(0, 0, 0, 1);
		glm::mat4 T1_5 = T1 * T2 * T3 * T4 * T5;

		
		scale = 200;
		for ( const auto& i : polygons ) {
			glColor3f(0,0,0);
			glBegin(GL_TRIANGLES);
				//cout << scale*get<0>(vertices3[get<0>(i)]) << scale*get<1>(vertices3[get<0>(i)]) << "\n";
				glm::vec4 A0 = glm::vec4(get<0>(vertices3[get<0>(i) - 1]), get<1>(vertices3[get<0>(i) - 1]), get<2>(vertices3[get<0>(i) - 1]), 1);
				glm::vec4 As = A0 * T1_5;
				GLfloat H = sqrt(pow(As[0] - get<0>(glediste), 2) + pow(As[1] - get<1>(glediste), 2) + pow(As[2] - get<2>(glediste), 2));
				GLfloat xp = (As[0] / As[2]) * H;
				GLfloat yp = (As[1] / As[2]) * H;
				glVertex2f(scale*xp + scale, scale*yp + scale);
				A0 = glm::vec4(get<0>(vertices3[get<1>(i) - 1]), get<1>(vertices3[get<1>(i) - 1]), get<2>(vertices3[get<1>(i) - 1]), 1);
				As = A0 * T1_5;
				H = sqrt(pow(As[0] - get<0>(glediste), 2) + pow(As[1] - get<1>(glediste), 2) + pow(As[2] - get<2>(glediste), 2));
				xp = (As[0] / As[2]) * H;
				yp = (As[1] / As[2]) * H;
				glVertex2f(scale*xp + scale, scale*yp + scale);
				A0 = glm::vec4(get<0>(vertices3[get<2>(i) - 1]), get<1>(vertices3[get<2>(i) - 1]), get<2>(vertices3[get<2>(i) - 1]), 1);
				As = A0 * T1_5;
				H = sqrt(pow(As[0] - get<0>(glediste), 2) + pow(As[1] - get<1>(glediste), 2) + pow(As[2] - get<2>(glediste), 2));
				xp = (As[0] / As[2]) * H;
				yp = (As[1] / As[2]) * H;
				glVertex2f(scale*xp + scale, scale*yp + scale);
			glEnd();
		}
		glFlush();
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	} 
	else if (theKey == 'D') {
		glm::mat4 T1;
		T1[0] = glm::vec4(1, 0, 0, - get<0>(ociste));
		T1[1] = glm::vec4(0, 1, 0, - get<1>(ociste));
		T1[2] = glm::vec4(0, 0, 1, - get<2>(ociste));
		T1[3] = glm::vec4(0, 0, 0, 1);
		GLfloat xg1 = get<0>(glediste) - get<0>(ociste);
		GLfloat yg1 = get<1>(glediste) - get<1>(ociste);
		GLfloat zg1 = get<2>(glediste) - get<2>(ociste);
		glm::mat4 T2;
		GLfloat sina = yg1/sqrt(pow(xg1, 2) + pow(yg1, 2));
		GLfloat cosa = xg1/sqrt(pow(xg1, 2) + pow(yg1, 2));
		T2[0] = glm::vec4(cosa, sina, 0, 0);
		T2[1] = glm::vec4(- sina, cosa, 0, 0);
		T2[2] = glm::vec4(0, 0, 1, 0);
		T2[3] = glm::vec4(0, 0, 0, 1);
		GLfloat xg2 = sqrt(pow(xg1,2) + pow(yg1, 2));
		GLfloat yg2 = 0;
		GLfloat zg2 = zg1;
		GLfloat sinb = xg2/sqrt(pow(xg2, 2) + pow(zg2, 2));
		GLfloat cosb = zg2/sqrt(pow(xg2, 2) + pow(zg2, 2));
		glm::mat4 T3;
		T3[0] = glm::vec4(cosb, 0, - sinb, 0);
		T3[1] = glm::vec4(0, 1, 0, 0);
		T3[2] = glm::vec4(sinb, 0, cosb, 0);
		T3[3] = glm::vec4(0, 0, 0, 1);
		GLfloat xg3 = 0;
		GLfloat yg3 = 0;
		GLfloat zg3 = sqrt(pow(xg2, 2) + pow(zg2, 2));
		glm::mat4 T4;
		T4[0] = glm::vec4(0, 1, 0, 0);
		T4[1] = glm::vec4(- 1, 0, 0, 0);
		T4[2] = glm::vec4(0, 0, 1, 0);
		T4[3] = glm::vec4(0, 0, 0, 1);
		glm::mat4 T5;
		T5[0] = glm::vec4(- 1, 0, 0, 0);
		T5[1] = glm::vec4(0, 1, 0, 0);
		T5[2] = glm::vec4(0, 0, 1, 0);
		T5[3] = glm::vec4(0, 0, 0, 1);
		glm::mat4 T1_5 = T1 * T2 * T3 * T4 * T5;

		
		scale = 200;
		for ( const auto& i : polygons ) {
			glColor3f(0,0,0);
			glBegin(GL_TRIANGLES);
				//cout << scale*get<0>(vertices3[get<0>(i)]) << scale*get<1>(vertices3[get<0>(i)]) << "\n";
				glm::vec4 A0 = glm::vec4(get<0>(vertices3[get<0>(i) - 1]), get<1>(vertices3[get<0>(i) - 1]), get<2>(vertices3[get<0>(i) - 1]), 1);
				glm::vec4 As = A0 * T1_5;
				GLfloat H = sqrt(pow(As[0] - get<0>(glediste), 2) + pow(As[1] - get<1>(glediste), 2) + pow(As[2] - get<2>(glediste), 2));
				GLfloat xp = (As[0] / As[2]) * H;
				GLfloat yp = (As[1] / As[2]) * H;
				glVertex2f(scale*xp + scale, scale*yp + scale);
				A0 = glm::vec4(get<0>(vertices3[get<1>(i) - 1]), get<1>(vertices3[get<1>(i) - 1]), get<2>(vertices3[get<1>(i) - 1]), 1);
				As = A0 * T1_5;
				H = sqrt(pow(As[0] - get<0>(glediste), 2) + pow(As[1] - get<1>(glediste), 2) + pow(As[2] - get<2>(glediste), 2));
				xp = (As[0] / As[2]) * H;
				yp = (As[1] / As[2]) * H;
				glVertex2f(scale*xp + scale, scale*yp + scale);
				A0 = glm::vec4(get<0>(vertices3[get<2>(i) - 1]), get<1>(vertices3[get<2>(i) - 1]), get<2>(vertices3[get<2>(i) - 1]), 1);
				As = A0 * T1_5;
				H = sqrt(pow(As[0] - get<0>(glediste), 2) + pow(As[1] - get<1>(glediste), 2) + pow(As[2] - get<2>(glediste), 2));
				xp = (As[0] / As[2]) * H;
				yp = (As[1] / As[2]) * H;
				glVertex2f(scale*xp + scale, scale*yp + scale);
			glEnd();
		}
		glFlush();
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); 
	} 
	else if (theKey == 'u') {
		get<0>(glediste) ++;
		glm::mat4 T1;
		T1[0] = glm::vec4(1, 0, 0, - get<0>(ociste));
		T1[1] = glm::vec4(0, 1, 0, - get<1>(ociste));
		T1[2] = glm::vec4(0, 0, 1, - get<2>(ociste));
		T1[3] = glm::vec4(0, 0, 0, 1);
		GLfloat xg1 = get<0>(glediste) - get<0>(ociste);
		GLfloat yg1 = get<1>(glediste) - get<1>(ociste);
		GLfloat zg1 = get<2>(glediste) - get<2>(ociste);
		glm::mat4 T2;
		GLfloat sina = yg1/sqrt(pow(xg1, 2) + pow(yg1, 2));
		GLfloat cosa = xg1/sqrt(pow(xg1, 2) + pow(yg1, 2));
		T2[0] = glm::vec4(cosa, sina, 0, 0);
		T2[1] = glm::vec4(- sina, cosa, 0, 0);
		T2[2] = glm::vec4(0, 0, 1, 0);
		T2[3] = glm::vec4(0, 0, 0, 1);
		GLfloat xg2 = sqrt(pow(xg1,2) + pow(yg1, 2));
		GLfloat yg2 = 0;
		GLfloat zg2 = zg1;
		GLfloat sinb = xg2/sqrt(pow(xg2, 2) + pow(zg2, 2));
		GLfloat cosb = zg2/sqrt(pow(xg2, 2) + pow(zg2, 2));
		glm::mat4 T3;
		T3[0] = glm::vec4(cosb, 0, - sinb, 0);
		T3[1] = glm::vec4(0, 1, 0, 0);
		T3[2] = glm::vec4(sinb, 0, cosb, 0);
		T3[3] = glm::vec4(0, 0, 0, 1);
		GLfloat xg3 = 0;
		GLfloat yg3 = 0;
		GLfloat zg3 = sqrt(pow(xg2, 2) + pow(zg2, 2));
		glm::mat4 T4;
		T4[0] = glm::vec4(0, 1, 0, 0);
		T4[1] = glm::vec4(- 1, 0, 0, 0);
		T4[2] = glm::vec4(0, 0, 1, 0);
		T4[3] = glm::vec4(0, 0, 0, 1);
		glm::mat4 T5;
		T5[0] = glm::vec4(- 1, 0, 0, 0);
		T5[1] = glm::vec4(0, 1, 0, 0);
		T5[2] = glm::vec4(0, 0, 1, 0);
		T5[3] = glm::vec4(0, 0, 0, 1);
		glm::mat4 T1_5 = T1 * T2 * T3 * T4 * T5;

		
		scale = 200;
		for ( const auto& i : polygons ) {
			glColor3f(0,0,0);
			glBegin(GL_TRIANGLES);
				//cout << scale*get<0>(vertices3[get<0>(i)]) << scale*get<1>(vertices3[get<0>(i)]) << "\n";
				glm::vec4 A0 = glm::vec4(get<0>(vertices3[get<0>(i) - 1]), get<1>(vertices3[get<0>(i) - 1]), get<2>(vertices3[get<0>(i) - 1]), 1);
				glm::vec4 As = A0 * T1_5;
				GLfloat H = sqrt(pow(As[0] - get<0>(glediste), 2) + pow(As[1] - get<1>(glediste), 2) + pow(As[2] - get<2>(glediste), 2));
				GLfloat xp = (As[0] / As[2]) * H;
				GLfloat yp = (As[1] / As[2]) * H;
				glVertex2f(scale*xp + scale, scale*yp + scale);
				A0 = glm::vec4(get<0>(vertices3[get<1>(i) - 1]), get<1>(vertices3[get<1>(i) - 1]), get<2>(vertices3[get<1>(i) - 1]), 1);
				As = A0 * T1_5;
				H = sqrt(pow(As[0] - get<0>(glediste), 2) + pow(As[1] - get<1>(glediste), 2) + pow(As[2] - get<2>(glediste), 2));
				xp = (As[0] / As[2]) * H;
				yp = (As[1] / As[2]) * H;
				glVertex2f(scale*xp + scale, scale*yp + scale);
				A0 = glm::vec4(get<0>(vertices3[get<2>(i) - 1]), get<1>(vertices3[get<2>(i) - 1]), get<2>(vertices3[get<2>(i) - 1]), 1);
				As = A0 * T1_5;
				H = sqrt(pow(As[0] - get<0>(glediste), 2) + pow(As[1] - get<1>(glediste), 2) + pow(As[2] - get<2>(glediste), 2));
				xp = (As[0] / As[2]) * H;
				yp = (As[1] / As[2]) * H;
				glVertex2f(scale*xp + scale, scale*yp + scale);
			glEnd();
		}
		glFlush();
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	} 
	else if (theKey == 'U') {
		get<0>(glediste) --;
		glm::mat4 T1;
		T1[0] = glm::vec4(1, 0, 0, - get<0>(ociste));
		T1[1] = glm::vec4(0, 1, 0, - get<1>(ociste));
		T1[2] = glm::vec4(0, 0, 1, - get<2>(ociste));
		T1[3] = glm::vec4(0, 0, 0, 1);
		GLfloat xg1 = get<0>(glediste) - get<0>(ociste);
		GLfloat yg1 = get<1>(glediste) - get<1>(ociste);
		GLfloat zg1 = get<2>(glediste) - get<2>(ociste);
		glm::mat4 T2;
		GLfloat sina = yg1/sqrt(pow(xg1, 2) + pow(yg1, 2));
		GLfloat cosa = xg1/sqrt(pow(xg1, 2) + pow(yg1, 2));
		T2[0] = glm::vec4(cosa, sina, 0, 0);
		T2[1] = glm::vec4(- sina, cosa, 0, 0);
		T2[2] = glm::vec4(0, 0, 1, 0);
		T2[3] = glm::vec4(0, 0, 0, 1);
		GLfloat xg2 = sqrt(pow(xg1,2) + pow(yg1, 2));
		GLfloat yg2 = 0;
		GLfloat zg2 = zg1;
		GLfloat sinb = xg2/sqrt(pow(xg2, 2) + pow(zg2, 2));
		GLfloat cosb = zg2/sqrt(pow(xg2, 2) + pow(zg2, 2));
		glm::mat4 T3;
		T3[0] = glm::vec4(cosb, 0, - sinb, 0);
		T3[1] = glm::vec4(0, 1, 0, 0);
		T3[2] = glm::vec4(sinb, 0, cosb, 0);
		T3[3] = glm::vec4(0, 0, 0, 1);
		GLfloat xg3 = 0;
		GLfloat yg3 = 0;
		GLfloat zg3 = sqrt(pow(xg2, 2) + pow(zg2, 2));
		glm::mat4 T4;
		T4[0] = glm::vec4(0, 1, 0, 0);
		T4[1] = glm::vec4(- 1, 0, 0, 0);
		T4[2] = glm::vec4(0, 0, 1, 0);
		T4[3] = glm::vec4(0, 0, 0, 1);
		glm::mat4 T5;
		T5[0] = glm::vec4(- 1, 0, 0, 0);
		T5[1] = glm::vec4(0, 1, 0, 0);
		T5[2] = glm::vec4(0, 0, 1, 0);
		T5[3] = glm::vec4(0, 0, 0, 1);
		glm::mat4 T1_5 = T1 * T2 * T3 * T4 * T5;

		
		scale = 200;
		for ( const auto& i : polygons ) {
			glColor3f(0,0,0);
			glBegin(GL_TRIANGLES);
				//cout << scale*get<0>(vertices3[get<0>(i)]) << scale*get<1>(vertices3[get<0>(i)]) << "\n";
				glm::vec4 A0 = glm::vec4(get<0>(vertices3[get<0>(i) - 1]), get<1>(vertices3[get<0>(i) - 1]), get<2>(vertices3[get<0>(i) - 1]), 1);
				glm::vec4 As = A0 * T1_5;
				GLfloat H = sqrt(pow(As[0] - get<0>(glediste), 2) + pow(As[1] - get<1>(glediste), 2) + pow(As[2] - get<2>(glediste), 2));
				GLfloat xp = (As[0] / As[2]) * H;
				GLfloat yp = (As[1] / As[2]) * H;
				glVertex2f(scale*xp + scale, scale*yp + scale);
				A0 = glm::vec4(get<0>(vertices3[get<1>(i) - 1]), get<1>(vertices3[get<1>(i) - 1]), get<2>(vertices3[get<1>(i) - 1]), 1);
				As = A0 * T1_5;
				H = sqrt(pow(As[0] - get<0>(glediste), 2) + pow(As[1] - get<1>(glediste), 2) + pow(As[2] - get<2>(glediste), 2));
				xp = (As[0] / As[2]) * H;
				yp = (As[1] / As[2]) * H;
				glVertex2f(scale*xp + scale, scale*yp + scale);
				A0 = glm::vec4(get<0>(vertices3[get<2>(i) - 1]), get<1>(vertices3[get<2>(i) - 1]), get<2>(vertices3[get<2>(i) - 1]), 1);
				As = A0 * T1_5;
				H = sqrt(pow(As[0] - get<0>(glediste), 2) + pow(As[1] - get<1>(glediste), 2) + pow(As[2] - get<2>(glediste), 2));
				xp = (As[0] / As[2]) * H;
				yp = (As[1] / As[2]) * H;
				glVertex2f(scale*xp + scale, scale*yp + scale);
			glEnd();
		}
		glFlush();
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	} 
	else if (theKey == 'i') {
		get<1>(glediste) ++;
		glm::mat4 T1;
		T1[0] = glm::vec4(1, 0, 0, - get<0>(ociste));
		T1[1] = glm::vec4(0, 1, 0, - get<1>(ociste));
		T1[2] = glm::vec4(0, 0, 1, - get<2>(ociste));
		T1[3] = glm::vec4(0, 0, 0, 1);
		GLfloat xg1 = get<0>(glediste) - get<0>(ociste);
		GLfloat yg1 = get<1>(glediste) - get<1>(ociste);
		GLfloat zg1 = get<2>(glediste) - get<2>(ociste);
		glm::mat4 T2;
		GLfloat sina = yg1/sqrt(pow(xg1, 2) + pow(yg1, 2));
		GLfloat cosa = xg1/sqrt(pow(xg1, 2) + pow(yg1, 2));
		T2[0] = glm::vec4(cosa, sina, 0, 0);
		T2[1] = glm::vec4(- sina, cosa, 0, 0);
		T2[2] = glm::vec4(0, 0, 1, 0);
		T2[3] = glm::vec4(0, 0, 0, 1);
		GLfloat xg2 = sqrt(pow(xg1,2) + pow(yg1, 2));
		GLfloat yg2 = 0;
		GLfloat zg2 = zg1;
		GLfloat sinb = xg2/sqrt(pow(xg2, 2) + pow(zg2, 2));
		GLfloat cosb = zg2/sqrt(pow(xg2, 2) + pow(zg2, 2));
		glm::mat4 T3;
		T3[0] = glm::vec4(cosb, 0, - sinb, 0);
		T3[1] = glm::vec4(0, 1, 0, 0);
		T3[2] = glm::vec4(sinb, 0, cosb, 0);
		T3[3] = glm::vec4(0, 0, 0, 1);
		GLfloat xg3 = 0;
		GLfloat yg3 = 0;
		GLfloat zg3 = sqrt(pow(xg2, 2) + pow(zg2, 2));
		glm::mat4 T4;
		T4[0] = glm::vec4(0, 1, 0, 0);
		T4[1] = glm::vec4(- 1, 0, 0, 0);
		T4[2] = glm::vec4(0, 0, 1, 0);
		T4[3] = glm::vec4(0, 0, 0, 1);
		glm::mat4 T5;
		T5[0] = glm::vec4(- 1, 0, 0, 0);
		T5[1] = glm::vec4(0, 1, 0, 0);
		T5[2] = glm::vec4(0, 0, 1, 0);
		T5[3] = glm::vec4(0, 0, 0, 1);
		glm::mat4 T1_5 = T1 * T2 * T3 * T4 * T5;

		
		scale = 200;
		for ( const auto& i : polygons ) {
			glColor3f(0,0,0);
			glBegin(GL_TRIANGLES);
				//cout << scale*get<0>(vertices3[get<0>(i)]) << scale*get<1>(vertices3[get<0>(i)]) << "\n";
				glm::vec4 A0 = glm::vec4(get<0>(vertices3[get<0>(i) - 1]), get<1>(vertices3[get<0>(i) - 1]), get<2>(vertices3[get<0>(i) - 1]), 1);
				glm::vec4 As = A0 * T1_5;
				GLfloat H = sqrt(pow(As[0] - get<0>(glediste), 2) + pow(As[1] - get<1>(glediste), 2) + pow(As[2] - get<2>(glediste), 2));
				GLfloat xp = (As[0] / As[2]) * H;
				GLfloat yp = (As[1] / As[2]) * H;
				glVertex2f(scale*xp + scale, scale*yp + scale);
				A0 = glm::vec4(get<0>(vertices3[get<1>(i) - 1]), get<1>(vertices3[get<1>(i) - 1]), get<2>(vertices3[get<1>(i) - 1]), 1);
				As = A0 * T1_5;
				H = sqrt(pow(As[0] - get<0>(glediste), 2) + pow(As[1] - get<1>(glediste), 2) + pow(As[2] - get<2>(glediste), 2));
				xp = (As[0] / As[2]) * H;
				yp = (As[1] / As[2]) * H;
				glVertex2f(scale*xp + scale, scale*yp + scale);
				A0 = glm::vec4(get<0>(vertices3[get<2>(i) - 1]), get<1>(vertices3[get<2>(i) - 1]), get<2>(vertices3[get<2>(i) - 1]), 1);
				As = A0 * T1_5;
				H = sqrt(pow(As[0] - get<0>(glediste), 2) + pow(As[1] - get<1>(glediste), 2) + pow(As[2] - get<2>(glediste), 2));
				xp = (As[0] / As[2]) * H;
				yp = (As[1] / As[2]) * H;
				glVertex2f(scale*xp + scale, scale*yp + scale);
			glEnd();
		}
		glFlush();
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	}
	else if (theKey == 'I') {
		get<1>(glediste) --;
		glm::mat4 T1;
		T1[0] = glm::vec4(1, 0, 0, - get<0>(ociste));
		T1[1] = glm::vec4(0, 1, 0, - get<1>(ociste));
		T1[2] = glm::vec4(0, 0, 1, - get<2>(ociste));
		T1[3] = glm::vec4(0, 0, 0, 1);
		GLfloat xg1 = get<0>(glediste) - get<0>(ociste);
		GLfloat yg1 = get<1>(glediste) - get<1>(ociste);
		GLfloat zg1 = get<2>(glediste) - get<2>(ociste);
		glm::mat4 T2;
		GLfloat sina = yg1/sqrt(pow(xg1, 2) + pow(yg1, 2));
		GLfloat cosa = xg1/sqrt(pow(xg1, 2) + pow(yg1, 2));
		T2[0] = glm::vec4(cosa, sina, 0, 0);
		T2[1] = glm::vec4(- sina, cosa, 0, 0);
		T2[2] = glm::vec4(0, 0, 1, 0);
		T2[3] = glm::vec4(0, 0, 0, 1);
		GLfloat xg2 = sqrt(pow(xg1,2) + pow(yg1, 2));
		GLfloat yg2 = 0;
		GLfloat zg2 = zg1;
		GLfloat sinb = xg2/sqrt(pow(xg2, 2) + pow(zg2, 2));
		GLfloat cosb = zg2/sqrt(pow(xg2, 2) + pow(zg2, 2));
		glm::mat4 T3;
		T3[0] = glm::vec4(cosb, 0, - sinb, 0);
		T3[1] = glm::vec4(0, 1, 0, 0);
		T3[2] = glm::vec4(sinb, 0, cosb, 0);
		T3[3] = glm::vec4(0, 0, 0, 1);
		GLfloat xg3 = 0;
		GLfloat yg3 = 0;
		GLfloat zg3 = sqrt(pow(xg2, 2) + pow(zg2, 2));
		glm::mat4 T4;
		T4[0] = glm::vec4(0, 1, 0, 0);
		T4[1] = glm::vec4(- 1, 0, 0, 0);
		T4[2] = glm::vec4(0, 0, 1, 0);
		T4[3] = glm::vec4(0, 0, 0, 1);
		glm::mat4 T5;
		T5[0] = glm::vec4(- 1, 0, 0, 0);
		T5[1] = glm::vec4(0, 1, 0, 0);
		T5[2] = glm::vec4(0, 0, 1, 0);
		T5[3] = glm::vec4(0, 0, 0, 1);
		glm::mat4 T1_5 = T1 * T2 * T3 * T4 * T5;

		
		scale = 200;
		for ( const auto& i : polygons ) {
			glColor3f(0,0,0);
			glBegin(GL_TRIANGLES);
				//cout << scale*get<0>(vertices3[get<0>(i)]) << scale*get<1>(vertices3[get<0>(i)]) << "\n";
				glm::vec4 A0 = glm::vec4(get<0>(vertices3[get<0>(i) - 1]), get<1>(vertices3[get<0>(i) - 1]), get<2>(vertices3[get<0>(i) - 1]), 1);
				glm::vec4 As = A0 * T1_5;
				GLfloat H = sqrt(pow(As[0] - get<0>(glediste), 2) + pow(As[1] - get<1>(glediste), 2) + pow(As[2] - get<2>(glediste), 2));
				GLfloat xp = (As[0] / As[2]) * H;
				GLfloat yp = (As[1] / As[2]) * H;
				glVertex2f(scale*xp + scale, scale*yp + scale);
				A0 = glm::vec4(get<0>(vertices3[get<1>(i) - 1]), get<1>(vertices3[get<1>(i) - 1]), get<2>(vertices3[get<1>(i) - 1]), 1);
				As = A0 * T1_5;
				H = sqrt(pow(As[0] - get<0>(glediste), 2) + pow(As[1] - get<1>(glediste), 2) + pow(As[2] - get<2>(glediste), 2));
				xp = (As[0] / As[2]) * H;
				yp = (As[1] / As[2]) * H;
				glVertex2f(scale*xp + scale, scale*yp + scale);
				A0 = glm::vec4(get<0>(vertices3[get<2>(i) - 1]), get<1>(vertices3[get<2>(i) - 1]), get<2>(vertices3[get<2>(i) - 1]), 1);
				As = A0 * T1_5;
				H = sqrt(pow(As[0] - get<0>(glediste), 2) + pow(As[1] - get<1>(glediste), 2) + pow(As[2] - get<2>(glediste), 2));
				xp = (As[0] / As[2]) * H;
				yp = (As[1] / As[2]) * H;
				glVertex2f(scale*xp + scale, scale*yp + scale);
			glEnd();
		}
		glFlush();
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	} 
	else if (theKey == 'o') {
		get<2>(glediste) ++;
		glm::mat4 T1;
		T1[0] = glm::vec4(1, 0, 0, - get<0>(ociste));
		T1[1] = glm::vec4(0, 1, 0, - get<1>(ociste));
		T1[2] = glm::vec4(0, 0, 1, - get<2>(ociste));
		T1[3] = glm::vec4(0, 0, 0, 1);
		GLfloat xg1 = get<0>(glediste) - get<0>(ociste);
		GLfloat yg1 = get<1>(glediste) - get<1>(ociste);
		GLfloat zg1 = get<2>(glediste) - get<2>(ociste);
		glm::mat4 T2;
		GLfloat sina = yg1/sqrt(pow(xg1, 2) + pow(yg1, 2));
		GLfloat cosa = xg1/sqrt(pow(xg1, 2) + pow(yg1, 2));
		T2[0] = glm::vec4(cosa, sina, 0, 0);
		T2[1] = glm::vec4(- sina, cosa, 0, 0);
		T2[2] = glm::vec4(0, 0, 1, 0);
		T2[3] = glm::vec4(0, 0, 0, 1);
		GLfloat xg2 = sqrt(pow(xg1,2) + pow(yg1, 2));
		GLfloat yg2 = 0;
		GLfloat zg2 = zg1;
		GLfloat sinb = xg2/sqrt(pow(xg2, 2) + pow(zg2, 2));
		GLfloat cosb = zg2/sqrt(pow(xg2, 2) + pow(zg2, 2));
		glm::mat4 T3;
		T3[0] = glm::vec4(cosb, 0, - sinb, 0);
		T3[1] = glm::vec4(0, 1, 0, 0);
		T3[2] = glm::vec4(sinb, 0, cosb, 0);
		T3[3] = glm::vec4(0, 0, 0, 1);
		GLfloat xg3 = 0;
		GLfloat yg3 = 0;
		GLfloat zg3 = sqrt(pow(xg2, 2) + pow(zg2, 2));
		glm::mat4 T4;
		T4[0] = glm::vec4(0, 1, 0, 0);
		T4[1] = glm::vec4(- 1, 0, 0, 0);
		T4[2] = glm::vec4(0, 0, 1, 0);
		T4[3] = glm::vec4(0, 0, 0, 1);
		glm::mat4 T5;
		T5[0] = glm::vec4(- 1, 0, 0, 0);
		T5[1] = glm::vec4(0, 1, 0, 0);
		T5[2] = glm::vec4(0, 0, 1, 0);
		T5[3] = glm::vec4(0, 0, 0, 1);
		glm::mat4 T1_5 = T1 * T2 * T3 * T4 * T5;

		
		scale = 200;
		for ( const auto& i : polygons ) {
			glColor3f(0,0,0);
			glBegin(GL_TRIANGLES);
				//cout << scale*get<0>(vertices3[get<0>(i)]) << scale*get<1>(vertices3[get<0>(i)]) << "\n";
				glm::vec4 A0 = glm::vec4(get<0>(vertices3[get<0>(i) - 1]), get<1>(vertices3[get<0>(i) - 1]), get<2>(vertices3[get<0>(i) - 1]), 1);
				glm::vec4 As = A0 * T1_5;
				GLfloat H = sqrt(pow(As[0] - get<0>(glediste), 2) + pow(As[1] - get<1>(glediste), 2) + pow(As[2] - get<2>(glediste), 2));
				GLfloat xp = (As[0] / As[2]) * H;
				GLfloat yp = (As[1] / As[2]) * H;
				glVertex2f(scale*xp + scale, scale*yp + scale);
				A0 = glm::vec4(get<0>(vertices3[get<1>(i) - 1]), get<1>(vertices3[get<1>(i) - 1]), get<2>(vertices3[get<1>(i) - 1]), 1);
				As = A0 * T1_5;
				H = sqrt(pow(As[0] - get<0>(glediste), 2) + pow(As[1] - get<1>(glediste), 2) + pow(As[2] - get<2>(glediste), 2));
				xp = (As[0] / As[2]) * H;
				yp = (As[1] / As[2]) * H;
				glVertex2f(scale*xp + scale, scale*yp + scale);
				A0 = glm::vec4(get<0>(vertices3[get<2>(i) - 1]), get<1>(vertices3[get<2>(i) - 1]), get<2>(vertices3[get<2>(i) - 1]), 1);
				As = A0 * T1_5;
				H = sqrt(pow(As[0] - get<0>(glediste), 2) + pow(As[1] - get<1>(glediste), 2) + pow(As[2] - get<2>(glediste), 2));
				xp = (As[0] / As[2]) * H;
				yp = (As[1] / As[2]) * H;
				glVertex2f(scale*xp + scale, scale*yp + scale);
			glEnd();
		}
		glFlush();
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	} 
	else if (theKey == 'O') {
		get<2>(glediste) --;
		glm::mat4 T1;
		T1[0] = glm::vec4(1, 0, 0, - get<0>(ociste));
		T1[1] = glm::vec4(0, 1, 0, - get<1>(ociste));
		T1[2] = glm::vec4(0, 0, 1, - get<2>(ociste));
		T1[3] = glm::vec4(0, 0, 0, 1);
		GLfloat xg1 = get<0>(glediste) - get<0>(ociste);
		GLfloat yg1 = get<1>(glediste) - get<1>(ociste);
		GLfloat zg1 = get<2>(glediste) - get<2>(ociste);
		glm::mat4 T2;
		GLfloat sina = yg1/sqrt(pow(xg1, 2) + pow(yg1, 2));
		GLfloat cosa = xg1/sqrt(pow(xg1, 2) + pow(yg1, 2));
		T2[0] = glm::vec4(cosa, sina, 0, 0);
		T2[1] = glm::vec4(- sina, cosa, 0, 0);
		T2[2] = glm::vec4(0, 0, 1, 0);
		T2[3] = glm::vec4(0, 0, 0, 1);
		GLfloat xg2 = sqrt(pow(xg1,2) + pow(yg1, 2));
		GLfloat yg2 = 0;
		GLfloat zg2 = zg1;
		GLfloat sinb = xg2/sqrt(pow(xg2, 2) + pow(zg2, 2));
		GLfloat cosb = zg2/sqrt(pow(xg2, 2) + pow(zg2, 2));
		glm::mat4 T3;
		T3[0] = glm::vec4(cosb, 0, - sinb, 0);
		T3[1] = glm::vec4(0, 1, 0, 0);
		T3[2] = glm::vec4(sinb, 0, cosb, 0);
		T3[3] = glm::vec4(0, 0, 0, 1);
		GLfloat xg3 = 0;
		GLfloat yg3 = 0;
		GLfloat zg3 = sqrt(pow(xg2, 2) + pow(zg2, 2));
		glm::mat4 T4;
		T4[0] = glm::vec4(0, 1, 0, 0);
		T4[1] = glm::vec4(- 1, 0, 0, 0);
		T4[2] = glm::vec4(0, 0, 1, 0);
		T4[3] = glm::vec4(0, 0, 0, 1);
		glm::mat4 T5;
		T5[0] = glm::vec4(- 1, 0, 0, 0);
		T5[1] = glm::vec4(0, 1, 0, 0);
		T5[2] = glm::vec4(0, 0, 1, 0);
		T5[3] = glm::vec4(0, 0, 0, 1);
		glm::mat4 T1_5 = T1 * T2 * T3 * T4 * T5;

		
		scale = 200;
		for ( const auto& i : polygons ) {
			glColor3f(0,0,0);
			glBegin(GL_TRIANGLES);
				//cout << scale*get<0>(vertices3[get<0>(i)]) << scale*get<1>(vertices3[get<0>(i)]) << "\n";
				glm::vec4 A0 = glm::vec4(get<0>(vertices3[get<0>(i) - 1]), get<1>(vertices3[get<0>(i) - 1]), get<2>(vertices3[get<0>(i) - 1]), 1);
				glm::vec4 As = A0 * T1_5;
				GLfloat H = sqrt(pow(As[0] - get<0>(glediste), 2) + pow(As[1] - get<1>(glediste), 2) + pow(As[2] - get<2>(glediste), 2));
				GLfloat xp = (As[0] / As[2]) * H;
				GLfloat yp = (As[1] / As[2]) * H;
				glVertex2f(scale*xp + scale, scale*yp + scale);
				A0 = glm::vec4(get<0>(vertices3[get<1>(i) - 1]), get<1>(vertices3[get<1>(i) - 1]), get<2>(vertices3[get<1>(i) - 1]), 1);
				As = A0 * T1_5;
				H = sqrt(pow(As[0] - get<0>(glediste), 2) + pow(As[1] - get<1>(glediste), 2) + pow(As[2] - get<2>(glediste), 2));
				xp = (As[0] / As[2]) * H;
				yp = (As[1] / As[2]) * H;
				glVertex2f(scale*xp + scale, scale*yp + scale);
				A0 = glm::vec4(get<0>(vertices3[get<2>(i) - 1]), get<1>(vertices3[get<2>(i) - 1]), get<2>(vertices3[get<2>(i) - 1]), 1);
				As = A0 * T1_5;
				H = sqrt(pow(As[0] - get<0>(glediste), 2) + pow(As[1] - get<1>(glediste), 2) + pow(As[2] - get<2>(glediste), 2));
				xp = (As[0] / As[2]) * H;
				yp = (As[1] / As[2]) * H;
				glVertex2f(scale*xp + scale, scale*yp + scale);
			glEnd();
		}
		glFlush();
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	} 
}   