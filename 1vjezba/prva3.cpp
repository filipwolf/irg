#include <glm/glm.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include "glm/ext.hpp"
#include <iostream>

int main() {
    int A1, B1, C1, A2, B2, C2, A3, B3, C3, x, y, z;

    std::cin >> A1 >> A2 >> A3;
    std::cin >> B1 >> B2 >> B3;
    std::cin >> C1 >> C2 >> C3;
    std::cin >> x >> y >> z;
    glm::vec3 vec1 = glm::vec3(A1, A2, A3);
    glm::vec3 vec2 = glm::vec3(B1, B2, B3);
    glm::vec3 vec3 = glm::vec3(C1, C2, C3);
    glm::mat3 mat1;
    mat1[0] = vec1;
    mat1[1] = vec2;
    mat1[2] = vec3;
    glm::vec3 B = glm::vec3(x, y, z);
    mat1 = glm::inverse(mat1);
    glm::vec3 result = mat1 * B;
    std::cout << glm::to_string(result) << "\n";
}