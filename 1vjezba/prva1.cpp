#include <glm/glm.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include "glm/ext.hpp"
#include <iostream>

int main () {
    glm::vec3 a = glm::vec3(2, 3, -4);
    glm::vec3 b = glm::vec3(-1, 4, -1);

    glm::vec3 c = a + b;
    std::cout << glm::to_string(a) << "\n";
    std::cout << glm::to_string(b) << "\n";
    std::cout << glm::to_string(c) << "\n";
    std::cout << "\n";
    glm::vec3 d = c*b;
    std::cout << glm::to_string(d) << "\n";
    std::cout << "\n";
    glm::vec3 e = glm::vec3(2, 2, 4);
    d = glm::cross(c,e);
    std::cout << glm::to_string(d) << "\n";
    std::cout << "\n";
    float f = glm::length(d);
    std::cout << f << "\n";
    std::cout << "\n";
    a = -d;
    std::cout << glm::to_string(a) << "\n";
    std::cout << "\n";
    glm::mat3 m1;
    m1[0] = glm::vec3(1, 2, 4);
    m1[1] = glm::vec3(2, 1, 5);
    m1[2] = glm::vec3(3, 3, 1);
    glm::mat3 m2;
    m2[0] = glm::vec3(-1, 5, -4);
    m2[1] = glm::vec3(2, -2, -1);
    m2[2] = glm::vec3(-3, 7, 3);
    glm::mat3 m3 = m1 + m2;
    m3 = glm::transpose(m3);
    std::cout << glm::to_string(m3[0]) << "\n";
    std::cout << glm::to_string(m3[1]) << "\n";
    std::cout << glm::to_string(m3[2]) << "\n";
    std::cout << "\n";
    m3 = m1 * glm::transpose(m2);
    m3 = glm::transpose(m3);
    std::cout << glm::to_string(m3[0]) << "\n";
    std::cout << glm::to_string(m3[1]) << "\n";
    std::cout << glm::to_string(m3[2]) << "\n";
    std::cout << "\n";
    m3 = m1 * glm::inverse(m2);
    m3 = glm::transpose(m3);
    std::cout << glm::to_string(m3[0]) << "\n";
    std::cout << glm::to_string(m3[1]) << "\n";
    std::cout << glm::to_string(m3[2]) << "\n";
}