//g++ -o konveksan_poligon konveksan_poligon.cpp -lglut -lGL
//odredi broj tocaka i nacrtaj konveksni poligon
//pritisni opet za bojanje

#include <stdio.h>
#include <iostream>
#include<GL/glut.h>

GLint n;
GLdouble Lx[1000], Ly[1000];
GLdouble a[1000], b[1000], c[1000];
GLint Ix;
GLint Vx, Vy;
bool bigFlag = false;

GLuint window;
GLuint width = 300, height = 300;
GLint ymin, ymax, xmin, xmax;

void myDisplay();
void myReshape(int width, int height);
void myMouse(int button, int state, int x, int y);
void myLine(GLint xa, GLint ya, GLint xb, GLint yb);

int main(int argc, char ** argv)
{
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	glutInitWindowSize(width, height);
	glutInitWindowPosition(100, 100);
	glutInit(&argc, argv);

	window = glutCreateWindow("Glut OpenGL konveksan poligon");
	glutReshapeFunc(myReshape);
	glutDisplayFunc(myDisplay);
	glutMouseFunc(myMouse);
	printf("Lijevom tipkom misa zadaj tocke\n");
    printf("Upisite zeljeni broj vrhova (max 1000): ");
    std::cin >> n;
		
	glutMainLoop();
	return 0;
}

void myDisplay()
{
	glFlush();
}

void myReshape(int w, int h)
{
	//printf("Pozvan myReshape()\n");
	width = w; height = h;               //promjena sirine i visine prozora
	Ix = 0;								//	indeks tocke 0-prva 1-druga tocka
	glViewport(0, 0, width, height);	//  otvor u prozoru
	
	glMatrixMode(GL_PROJECTION);		//	matrica projekcije
	glLoadIdentity();					//	jedinicna matrica
	glOrtho(0, width - 1, height - 1, 0, 0, 1); 	//	okomita projekcija
	glMatrixMode(GL_MODELVIEW);			//	matrica pogleda
	glLoadIdentity();					//	jedinicna matrica

	glClearColor(1.0f, 1.0f, 1.0f, 0.0f); // boja pozadine
	glClear(GL_COLOR_BUFFER_BIT);		//	brisanje pozadine
	glPointSize(1.0);					//	postavi velicinu tocke za liniju
	glColor3f(0.0f, 0.0f, 0.0f);		//	postavi boju linije
}

void myLine(GLint xa, GLint ya, GLint xb, GLint yb)
{
	// glBegin(GL_LINES);
	// {
	// 	glVertex2i(xa, ya + 20);			//	crtanje gotove linije
	// 	glVertex2i(xb, yb + 20);
	// }
	// glEnd();

	GLint	x;                        		//	Bresenhamov algoritam do 45  
	GLint	y;
	GLint korekcija, a, d;
	if (xa <= xb) {
		if (ya <= yb) {
			if (yb - ya <= xb - xa) {
				y = ya;
				a = 2*(yb - ya);
				d = - (xb - xa);
				korekcija = -2*(xb - xa);

				glBegin(GL_POINTS);
				for (x = xa; x<=xb; x++)
				{
					glVertex2i(x, y);
					d = d + a;
					if (d > 0)
					{
						y++;
						d = d + korekcija;
					}
				}
				glEnd();
			} else {
				x = xb; xb = yb; yb = x;
				x = xa; xa = ya; ya = x;
				y = ya;
				a = 2*(yb - ya);
				d = - (xb - xa);
				korekcija = -2*(xb - xa);

				glBegin(GL_POINTS);
				for (x = xa; x<=xb; x++)
				{
					glVertex2i(y, x);
					d = d + a;
					if (d > 0)
					{
						y++;
						d = d + korekcija;
					}
				}
				glEnd();
			}
		} else {
			if (- (yb - ya) <= xb - xa) {
				y = ya;
				a = 2*(yb - ya);
				d = (xb - xa);
				korekcija = 2*(xb - xa);

				glBegin(GL_POINTS);
				for (x = xa; x<=xb; x++)
				{
					glVertex2i(x, y);
					d = d + a;
					if (d <= 0)
					{
						y--;
						d = d + korekcija;
					}
				}
				glEnd();
			} else {
				x = xb; xb = ya; ya = x;
				x = xa; xa = yb; yb = x;
				y = ya;
				a = 2*(yb - ya);
				d = (xb - xa);
				korekcija = 2*(xb - xa);

				glBegin(GL_POINTS);
				for (x = xa; x<=xb; x++)
				{
					glVertex2i(y, x);
					d = d + a;
					if (d <= 0)
					{
						y--;
						d = d + korekcija;
					}
				}
				glEnd();
			}
		}
	} else {
		GLint n = xa; xa = xb; xb = n; n = ya; ya = yb; yb = n;
		if (yb >= ya) {
			if (yb - ya <= xb - xa) {
				y = ya;
				a = 2*(yb - ya);
				d = - (xb - xa);
				korekcija = -2*(xb - xa);

				glBegin(GL_POINTS);
				for (x = xa; x<=xb; x++)
				{
					glVertex2i(x, y);
					d = d + a;
					if (d > 0)
					{
						y++;
						d = d + korekcija;
					}
				}
				glEnd();
			} else {
				x = xb; xb = yb; yb = x;
				x = xa; xa = ya; ya = x;
				y = ya;
				a = 2*(yb - ya);
				d = - (xb - xa);
				korekcija = -2*(xb - xa);

				glBegin(GL_POINTS);
				for (x = xa; x<=xb; x++)
				{
					glVertex2i(y, x);
					d = d + a;
					if (d > 0)
					{
						y++;
						d = d + korekcija;
					}
				}
				glEnd();
			}
		} else {
			//GLint n = xa; xa = xb; xb = n; n = ya; ya = yb; yb = n;
			if (- (yb - ya) <= xb - xa) {
				y = ya;
				a = 2*(yb - ya);
				d = (xb - xa);
				korekcija = 2*(xb - xa);

				glBegin(GL_POINTS);
				for (x = xa; x<=xb; x++)
				{
					glVertex2i(x, y);
					d = d + a;
					if (d <= 0)
					{
						y--;
						d = d + korekcija;
					}
				}
				glEnd();
			} else {
				x = xb; xb = ya; ya = x;
				x = xa; xa = yb; yb = x;
				y = ya;
				a = 2*(yb - ya);
				d = (xb - xa);
				korekcija = 2*(xb - xa);

				glBegin(GL_POINTS);
				for (x = xa; x<=xb; x++)
				{
					glVertex2i(y, x);
					d = d + a;
					if (d <= 0)
					{
						y--;
						d = d + korekcija;
					}
				}
				glEnd();
			}
		}
	}
}

//*********************************************************************************
//	Mis.
//*********************************************************************************

void myMouse(int button, int state, int x, int y)
{
	if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) {

        if (bigFlag == true) {
            for (int y0 = ymin; y0 <= ymax; y0++) {
                int L = xmin;
                int D = xmax;
                for (int i = 0; i < n; i++) {
                    if (a[i] != 0) {
                        int x1 = (-b[i]*y0 - c[i])/a[i];
                        if (Ly[i] >= Ly[i + 1] && x1 > L) L = x1;
                        if (Ly[i] < Ly[i + 1] && x1 < D) D = x1;
                        //printf("%d, %d\n", L, D);
                    }
                }
                if (L < D) {
                    glBegin(GL_LINES);
                    {
                        glVertex2i(L, y0);
                        glVertex2i(D, y0);
                    }
                    glEnd();
                }
            }
        } else {
            if (Ix < n) {
                Lx[Ix] = x;
                Ly[Ix] = y;
                Ix ++;
                if (Ix == n) {
                    ymin = Ly[0];
                    ymax = Ly[0];
                    xmin = Ly[0];   
                    xmax = Ly[0];
                    Ly[n] = Ly[0];
                    Lx[n] = Lx[0];
                    for (int i = 1; i < n; i++) {
                        if (Lx[i] > xmax) xmax = Lx[i];
                        if (Lx[i] < xmin) xmin = Lx[i];
                        if (Ly[i] > ymax) ymax = Ly[i];
                        if (Ly[i] < ymin) ymin = Ly[i];
                    }
                    for (int i = 0; i < n; i++) {
                        a[i] = Ly[i] - Ly[i + 1];
                        b[i] = -Lx[i] + Lx[i + 1];
                        c[i] = Lx[i]*Ly[i + 1] - Lx[i + 1]*Ly[i];
                        //std::cout << a[i] << "\n" << b[i] << "\n" << c[i] << "\n" << ymax << "\n";
                    }
                    //std::cout << xmin << "\n" << xmax << "\n" << ymin << "\n" << ymax << "\n";
                    printf("Koordinate tocke %d: %d %d \n", Ix, x, y);
                    for (int i = 1; i < n; i++) {
                        myLine((int)Lx[i], (int)Ly[i], (int)Lx[i - 1], (int)Ly[i - 1]);
                        if (i == n - 1) {
                            myLine((int)Lx[i], (int)Ly[i], (int)Lx[0], (int)Ly[0]);
                            //Ix = 0;
                        }
                    } 
                } else {
                    printf("Koordinate tocke %d: %d %d \n", Ix, x, y);
                }
            } else {
                Vx = x;
                Vy = y;
                bool flag = false;
                printf("Tocka V je %d %d \n", Vx, Vy);
                for (int i = 0; i < n; i++) {
                    int nekaj = x*a[i] + y*b[i] + c[i];
                    //printf("%d\n", nekaj);
                    if (nekaj < 0){
                        printf("Tocka V je izvan poligona \n");
                        flag = true;
                        break;
                    }
                }
                if (flag == false) printf("Tocka V je unutar poligona \n");
                printf("Prtisni mis za bojanje\n");
                bigFlag = true;
            }
        }

		glFlush();
	}
}

