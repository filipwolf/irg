//g++ -o konveksan_poligon konveksan_poligon.cpp -lglut -lGL
//odmah pritisnuti c
//upisati naziv datoteke
//upisati koordinate tocke
//unijeti skaliranje

#include <stdio.h>
#include <iostream>
#include<GL/glut.h>
#include <fstream>
#include <bits/stdc++.h>
#include <boost/algorithm/string.hpp>

using namespace std;

GLint n;
GLdouble Lx[1000], Ly[1000];
GLdouble a[1000], b[1000], c[1000];
GLint Ix;
float Vx, Vy, Vz;
bool bigFlag = false;

GLuint window;
GLuint width = 1000, height = 800;
GLint ymin, ymax, xmin, xmax;

void myDisplay();
void myReshape(int width, int height);
void myKeyboard(unsigned char theKey, int mouseX, int mouseY);

int main(int argc, char ** argv)
{
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	glutInitWindowSize(width, height);
	glutInitWindowPosition(100, 100);
	glutInit(&argc, argv);

	window = glutCreateWindow("Glut OpenGL tijelo");
	glutReshapeFunc(myReshape);
	glutDisplayFunc(myDisplay);
    glutKeyboardFunc(myKeyboard);
    printf("Pritisni \"C\" za crtanje\n");
		
	glutMainLoop();
	return 0;
}

void myDisplay()
{
	glClear(GL_COLOR_BUFFER_BIT);
	glFlush();
}

void myReshape(int w, int h)
{
	//printf("Pozvan myReshape()\n");
	width = w; height = h;
	Ix = 0;
	glViewport(0, 0, width, height);
	
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0, width - 1, height - 1, 0, 0, 1);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glClearColor(1.0f, 1.0f, 1.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT);
	glPointSize(1.0);
	glColor3f(0.0f, 0.0f, 0.0f);
}

void myKeyboard(unsigned char theKey, int mouseX, int mouseY)
{
    ifstream object;
    string line;
	vector<tuple<float, float, float>> vertices;
	vector<tuple<float, float, float>> vertices2;
	vector<tuple<float, float, float>> vertices3;
	vector<tuple<int, int, int>> polygons;
    int vertexCnt = 0, polygonCnt = 0;
	float xmax, xmin, ymax, ymin, zmax, zmin;
	float max;
	float middlex, middley, middlez;
	float scale;
	string fileName;

	printf("Zadajte datoteku: ");
	cin >> fileName;
    object.open(fileName);
    while ( getline (object,line) ) {
		if (line[0] == 'v') {
			size_t pos = 0;
			string token;
			string delimiter = " ";
			vector<float> v;
			while ((pos = line.find(delimiter)) != string::npos) {
    			token = line.substr(0, pos);
				if (token.compare("v") == 0) {
					line.erase(0, pos + delimiter.length());
					continue;
				}
				v.push_back(stof(token));
    			line.erase(0, pos + delimiter.length());
			}
			vertices.push_back(make_tuple(v[0], v[1], stof(line)));
			vertexCnt++;
      	}
    	if (line[0] == 'f') {
			size_t pos = 0;
			string token;
			string delimiter = " ";
			vector<int> v;
			while ((pos = line.find(delimiter)) != string::npos) {
    			token = line.substr(0, pos);
				if (token.compare("f") == 0) {
					line.erase(0, pos + delimiter.length());
					continue;
				}
				v.push_back(stoi(token));
    			line.erase(0, pos + delimiter.length());
			}
			polygons.push_back(make_tuple(v[0], v[1], stoi(line)));
			polygonCnt++;
		}
    }
	object.close();
	xmax = get<0>(vertices[0]);
	xmin = get<0>(vertices[0]);
	ymax = get<1>(vertices[0]);
	ymin = get<1>(vertices[0]);
	zmax = get<2>(vertices[0]);
	zmin = get<2>(vertices[0]);
	for ( const auto& i : vertices ) {
		//cout << get<0>(i) << get<1>(i) << get<2>(i) << endl;
		if (get<0>(i) > xmax) xmax = get<0>(i);
		if (get<0>(i) < xmin) xmin = get<0>(i);
		if (get<1>(i) > ymax) ymax = get<1>(i);
		if (get<1>(i) < ymin) ymin = get<1>(i);
		if (get<2>(i) > zmax) zmax = get<2>(i);
		if (get<2>(i) < zmin) zmin = get<2>(i);
	}
	//cout << xmax << xmin << ymax << ymin << zmax << zmin << endl;
	middlex = (xmax + xmin) / 2.0;
	middley = (ymax + ymin) / 2.0;
	middlez = (zmax + zmin) / 2.0;
	if (xmax > ymax) max = xmax;
	else max = ymax;
	if (zmax > max) max = zmax;
	//cout << middlex << middley << middlez << "\n";
	for ( const auto& i : vertices ) {
		//cout << get<0>(i) << get<1>(i) << get<2>(i) << endl;
		vertices2.push_back(make_tuple(get<0>(i) - middlex, get<1>(i) - middley, get<2>(i) - middlez));
	}
	xmax = get<0>(vertices2[0]);
	xmin = get<0>(vertices2[0]);
	ymax = get<1>(vertices2[0]);
	ymin = get<1>(vertices2[0]);
	zmax = get<2>(vertices2[0]);
	zmin = get<2>(vertices2[0]);
	for ( const auto& i : vertices2 ) {
		//cout << get<0>(i) << get<1>(i) << get<2>(i) << endl;
		if (get<0>(i) > xmax) xmax = get<0>(i);
		if (get<0>(i) < xmin) xmin = get<0>(i);
		if (get<1>(i) > ymax) ymax = get<1>(i);
		if (get<1>(i) < ymin) ymin = get<1>(i);
		if (get<2>(i) > zmax) zmax = get<2>(i);
		if (get<2>(i) < zmin) zmin = get<2>(i);
	}
	//cout << xmax << xmin << ymax << ymin << zmax << zmin << endl;
	middlex = (xmax + xmin) / 2.0;
	middley = (ymax + ymin) / 2.0;
	middlez = (zmax + zmin) / 2.0;
	//cout << middlex << middley << middlez << "\n";
	if (xmax > ymax) max = xmax;
	else max = ymax;
	if (zmax > max) max = zmax;
	for ( const auto& i : vertices2 ) {
		//cout << get<0>(i)/max << get<1>(i)/max << get<2>(i)/max << endl;
		vertices3.push_back(make_tuple(get<0>(i)/max, get<1>(i)/max, get<2>(i)/max));
	}
	printf("Ucitaj koordinate tocke V: ");
	cin >> Vx >> Vy >> Vz;
	for ( const auto& i : polygons ) {
		float A = (get<1>(vertices3[get<1>(i) - 1]) - get<1>(vertices3[get<0>(i) - 1]))*(get<2>(vertices3[get<2>(i) - 1]) - get<2>(vertices3[get<0>(i) - 1]))
		- (get<2>(vertices3[get<1>(i) - 1]) - get<2>(vertices3[get<0>(i) - 1]))*(get<1>(vertices3[get<2>(i) - 1]) - get<1>(vertices3[get<0>(i) - 1]));

		float B = - (get<0>(vertices3[get<1>(i) - 1]) - get<0>(vertices3[get<0>(i) - 1]))*(get<2>(vertices3[get<2>(i) - 1]) - get<2>(vertices3[get<0>(i) - 1]))
		+ (get<2>(vertices3[get<1>(i) - 1]) - get<2>(vertices3[get<0>(i) - 1]))*(get<0>(vertices3[get<2>(i) - 1]) - get<0>(vertices3[get<0>(i) - 1]));

		float C = (get<0>(vertices3[get<1>(i) - 1]) - get<0>(vertices3[get<0>(i) - 1]))*(get<1>(vertices3[get<2>(i) - 1]) - get<1>(vertices3[get<0>(i) - 1]))
		- (get<1>(vertices3[get<1>(i) - 1]) - get<1>(vertices3[get<0>(i) - 1]))*(get<0>(vertices3[get<2>(i) - 1]) - get<0>(vertices3[get<0>(i) - 1]));

		float D = - get<0>(vertices3[get<0>(i) - 1])*A - get<1>(vertices3[get<0>(i) - 1])*B - get<2>(vertices3[get<0>(i) - 1])*C;

		if (A*Vx + B*Vy + C*Vz + D > 0) {
			printf("Tocka je izvan tijela\n");
			bigFlag = true;
			break;
		}
	}
	if (bigFlag == false) printf("Tocka je unutar tijela\n");
	printf("Unesite skaliranje: ");
	cin >> scale;
	for ( const auto& i : polygons ) {
		glColor3f(0,0,0);
		glBegin(GL_TRIANGLES);
			//cout << scale*get<0>(vertices3[get<0>(i)]) << scale*get<1>(vertices3[get<0>(i)]) << "\n";
			glVertex2f(scale*get<0>(vertices3[get<0>(i) - 1]) + scale, scale*get<1>(vertices3[get<0>(i) - 1]) + scale);
			glVertex2f(scale*get<0>(vertices3[get<1>(i) - 1]) + scale, scale*get<1>(vertices3[get<1>(i) - 1]) + scale);
			glVertex2f(scale*get<0>(vertices3[get<2>(i) - 1]) + scale, scale*get<1>(vertices3[get<2>(i) - 1]) + scale);
		glEnd();
	}

	glFlush();
}