//*************************************************************************************************************
//	Crtanje tijela
//	Tijelo se crta u funkciji myObject
//
//	Zadatak: Treba ucitati tijelo zapisano u *.obj, sjencati i ukloniti staznje poligone
//	S tastature l - pomicanje ocista po x osi +
//		k - pomicanje ocista po x osi +
//              r - pocetni polozaj
//              esc izlaz iz programa
//*************************************************************************************************************

#include <stdio.h>
#include <iostream>
#include <GL/glut.h>
#include <fstream>
#include <bits/stdc++.h>
#include <boost/algorithm/string.hpp>
#include <glm/glm.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include "glm/ext.hpp"

//*********************************************************************************
//	Pokazivac na glavni prozor i pocetna velicina.
//*********************************************************************************

using namespace std;

GLuint window;
GLuint width = 300, height = 300;
vector<tuple<float, float, float>> vertices;
vector<tuple<float, float, float>> vertices2;
vector<tuple<float, float, float>> vertices3;
vector<tuple<float, float, float>> normaleIzr;
vector<tuple<int, int, int>> polygons;
map<int, vector<int>> normale;
ifstream object;
string line;
int vertexCnt = 0, polygonCnt = 0;
float xmax, xmin, ymax, ymin, zmax, zmin;
float maxAll;
float middlex, middley, middlez;
float scale = 200;
string fileName;
string koords;
bool mode;

typedef struct _Ociste {
	GLdouble	x;
	GLdouble	y;
	GLdouble	z;
} Ociste;

Ociste ociste = {0, 0 ,5.0};
Ociste izvor;

//*********************************************************************************
//	Function Prototypes.
//*********************************************************************************

void myDisplay		();
void myReshape		(int width, int height);
void myMouse		(int button, int state, int x, int y);
void myKeyboard		(unsigned char theKey, int mouseX, int mouseY);
void myObject		();
void redisplay_all	(void);
void updatePerspective ();

//*********************************************************************************
//	Glavni program.
//*********************************************************************************

int main(int argc, char** argv)
{
	// postavljanje dvostrukog spremnika za prikaz (zbog titranja)
	glutInitDisplayMode (GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
	glutInitWindowSize (width, height);
	glutInitWindowPosition (100, 100);
	glutInit(&argc, argv);

	window = glutCreateWindow ("Tijelo");
	glutReshapeFunc(myReshape);
	glutDisplayFunc(myDisplay);
	glutMouseFunc(myMouse);
	glutKeyboardFunc(myKeyboard);
	printf("Tipka: l - pomicanje ocista po x os +\n");
	printf("Tipka: k - pomicanje ocista po x os -\n");
	printf("Tipka: r - pocetno stanje\n");
    printf("Tipka: c - ucitavanje objekta\n");
    printf("Tipka: i - upisi koordiate izvora\n");
	printf("esc: izlaz iz programa\n");

	glutMainLoop();
	return 0;
}

//*********************************************************************************
//	Osvjezavanje prikaza.
//*********************************************************************************

void myDisplay(void)
{
	// printf("Pozvan myDisplay()\n");
	glClearColor( 1.0f, 1.0f, 1.0f, 1.0f);		         // boja pozadine - bijela
	glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	myObject ();
	glutSwapBuffers();      // iscrtavanje iz dvostrukog spemnika (umjesto glFlush)
}

//*********************************************************************************
//	Promjena velicine prozora.
//	Funkcija gluPerspective i gluLookAt se obavlja
//		transformacija pogleda i projekcija
//*********************************************************************************

void myReshape (int w, int h)
{
	// printf("MR: width=%d, height=%d\n",w,h);
	width=w; height=h;
	glViewport (0, 0, width, height);
	updatePerspective();
}

void updatePerspective()
{
	glMatrixMode (GL_PROJECTION);        // aktivirana matrica projekcije
	glLoadIdentity ();
	gluPerspective(45.0, (float)width/height, 0.5, 8.0); // kut pogleda, x/y, prednja i straznja ravnina odsjecanja
	glMatrixMode (GL_MODELVIEW);         // aktivirana matrica modela
	glLoadIdentity ();
	gluLookAt (ociste.x, ociste.y, ociste.z, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);	// ociste x,y,z; glediste x,y,z; up vektor x,y,z
}

//*********************************************************************************
//	Crta moj objekt. Ovdje treba naciniti prikaz ucitanog objekta.
//*********************************************************************************

void myObject ()
{
	// glutWireCube (1.0);
	// glutSolidCube (1.0);
	// glutWireTeapot (1.0);
	// glutSolidTeapot (1.0);

	// glBegin (GL_TRIANGLES); // ili glBegin (GL_LINE_LOOP); za zicnu formu
	// 	glColor3ub(255, 0, 0);	glVertex3f(-1.0, 0.0, 0.0);
	// 	glColor3ub(0, 0, 0);	glVertex3f(0.0, 1.0, 0.0);
	// 	glColor3ub(100, 0, 0);	glVertex3f(0.0, 0.0, 1.0);
	// glEnd();
    int Ia = 10;
    float ka = 0.1;
    int Ii = 200;
    float kd = 0.7;
    glEnable(GL_DEPTH_TEST);
    if (mode) {
        for ( const auto& i : polygons ) {

            float A = (get<1>(vertices3[get<1>(i) - 1]) - get<1>(vertices3[get<0>(i) - 1]))*(get<2>(vertices3[get<2>(i) - 1]) - get<2>(vertices3[get<0>(i) - 1]))
            - (get<2>(vertices3[get<1>(i) - 1]) - get<2>(vertices3[get<0>(i) - 1]))*(get<1>(vertices3[get<2>(i) - 1]) - get<1>(vertices3[get<0>(i) - 1]));

            float B = - (get<0>(vertices3[get<1>(i) - 1]) - get<0>(vertices3[get<0>(i) - 1]))*(get<2>(vertices3[get<2>(i) - 1]) - get<2>(vertices3[get<0>(i) - 1]))
            + (get<2>(vertices3[get<1>(i) - 1]) - get<2>(vertices3[get<0>(i) - 1]))*(get<0>(vertices3[get<2>(i) - 1]) - get<0>(vertices3[get<0>(i) - 1]));

            float C = (get<0>(vertices3[get<1>(i) - 1]) - get<0>(vertices3[get<0>(i) - 1]))*(get<1>(vertices3[get<2>(i) - 1]) - get<1>(vertices3[get<0>(i) - 1]))
            - (get<1>(vertices3[get<1>(i) - 1]) - get<1>(vertices3[get<0>(i) - 1]))*(get<0>(vertices3[get<2>(i) - 1]) - get<0>(vertices3[get<0>(i) - 1]));

            float D = - get<0>(vertices3[get<0>(i) - 1])*A - get<1>(vertices3[get<0>(i) - 1])*B - get<2>(vertices3[get<0>(i) - 1])*C;

            float len1 = glm::length(glm::vec3(izvor.x, izvor.y, izvor.z));
            float len2 = glm::length(glm::vec3(A, B, C));
            float LN = max(double(0), ((A/len1)*(izvor.x/len2) + (B/len1)*(izvor.y/len2) + (C/len1)*(izvor.z/len2)));
            
            float Id = Ia*ka + Ii*kd*LN;

            if (A*ociste.x + B*ociste.y + C*ociste.z + D > 0) {
                glBegin(GL_TRIANGLES);
                    //cout << get<0>(vertices3[get<0>(i)]) << get<1>(vertices3[get<0>(i)]) << "\n";
                    glColor3ub(Id, 0, Id);
                    glVertex3f(get<0>(vertices3[get<0>(i) - 1]), get<1>(vertices3[get<0>(i) - 1]), get<2>(vertices3[get<0>(i) - 1]));
                    glColor3ub(Id, 0, Id);
                    glVertex3f(get<0>(vertices3[get<1>(i) - 1]), get<1>(vertices3[get<1>(i) - 1]), get<2>(vertices3[get<1>(i) - 1]));
                    glColor3ub(Id, 0, Id);
                    glVertex3f(get<0>(vertices3[get<2>(i) - 1]), get<1>(vertices3[get<2>(i) - 1]), get<2>(vertices3[get<2>(i) - 1]));
                glEnd();
            }
        }
    } else {
        for ( const auto& i : polygons ) {

            float A = (get<1>(vertices3[get<1>(i) - 1]) - get<1>(vertices3[get<0>(i) - 1]))*(get<2>(vertices3[get<2>(i) - 1]) - get<2>(vertices3[get<0>(i) - 1]))
            - (get<2>(vertices3[get<1>(i) - 1]) - get<2>(vertices3[get<0>(i) - 1]))*(get<1>(vertices3[get<2>(i) - 1]) - get<1>(vertices3[get<0>(i) - 1]));

            float B = - (get<0>(vertices3[get<1>(i) - 1]) - get<0>(vertices3[get<0>(i) - 1]))*(get<2>(vertices3[get<2>(i) - 1]) - get<2>(vertices3[get<0>(i) - 1]))
            + (get<2>(vertices3[get<1>(i) - 1]) - get<2>(vertices3[get<0>(i) - 1]))*(get<0>(vertices3[get<2>(i) - 1]) - get<0>(vertices3[get<0>(i) - 1]));

            float C = (get<0>(vertices3[get<1>(i) - 1]) - get<0>(vertices3[get<0>(i) - 1]))*(get<1>(vertices3[get<2>(i) - 1]) - get<1>(vertices3[get<0>(i) - 1]))
            - (get<1>(vertices3[get<1>(i) - 1]) - get<1>(vertices3[get<0>(i) - 1]))*(get<0>(vertices3[get<2>(i) - 1]) - get<0>(vertices3[get<0>(i) - 1]));

            float D = - get<0>(vertices3[get<0>(i) - 1])*A - get<1>(vertices3[get<0>(i) - 1])*B - get<2>(vertices3[get<0>(i) - 1])*C;

            float len1 = glm::length(glm::vec3(izvor.x, izvor.y, izvor.z));

            float len2 = glm::length(glm::vec3(get<0>(normaleIzr[get<0>(i) - 1]), get<1>(normaleIzr[get<0>(i) - 1]), get<2>(normaleIzr[get<0>(i) - 1])));

            float LN1 = max(double(0), double((get<0>(normaleIzr[get<0>(i) - 1])*izvor.x
            + get<1>(normaleIzr[get<0>(i) - 1])*izvor.y + get<2>(normaleIzr[get<0>(i) - 1])*izvor.z)/(len1*len2)));
            
            len2 = glm::length(glm::vec3(get<0>(normaleIzr[get<1>(i) - 1]), get<1>(normaleIzr[get<1>(i) - 1]), get<2>(normaleIzr[get<1>(i) - 1])));

            float LN2 = max(double(0), double((get<0>(normaleIzr[get<1>(i) - 1])*izvor.x
            + get<1>(normaleIzr[get<1>(i) - 1])*izvor.y + get<2>(normaleIzr[get<1>(i) - 1])*izvor.z)/(len1*len2)));

            len2 = glm::length(glm::vec3(get<0>(normaleIzr[get<2>(i) - 1]), get<1>(normaleIzr[get<2>(i) - 1]), get<2>(normaleIzr[get<2>(i) - 1])));

            float LN3 = max(double(0), double((get<0>(normaleIzr[get<2>(i) - 1])*izvor.x
            + get<1>(normaleIzr[get<2>(i) - 1])*izvor.y + get<2>(normaleIzr[get<2>(i) - 1])*izvor.z)/(len1*len2)));
            
            //cout << LN1 << " " << LN2 << " " << LN3 << "\n";

            float Id1 = Ia*ka + Ii*kd*LN1;
            float Id2 = Ia*ka + Ii*kd*LN2;
            float Id3 = Ia*ka + Ii*kd*LN3;

            if (A*ociste.x + B*ociste.y + C*ociste.z + D > 0) {
                glBegin(GL_TRIANGLES);
                    //cout << get<0>(vertices3[get<0>(i)]) << get<1>(vertices3[get<0>(i)]) << "\n";
                    glColor3ub(Id1, 0, Id1);
                    glVertex3f(get<0>(vertices3[get<0>(i) - 1]), get<1>(vertices3[get<0>(i) - 1]), get<2>(vertices3[get<0>(i) - 1]));
                    glColor3ub(Id2, 0, Id2);
                    glVertex3f(get<0>(vertices3[get<1>(i) - 1]), get<1>(vertices3[get<1>(i) - 1]), get<2>(vertices3[get<1>(i) - 1]));
                    glColor3ub(Id3, 0, Id3);
                    glVertex3f(get<0>(vertices3[get<2>(i) - 1]), get<1>(vertices3[get<2>(i) - 1]), get<2>(vertices3[get<2>(i) - 1]));
                glEnd();
            }
        }
    }
}

//*********************************************************************************
//	Mis.
//*********************************************************************************

void myMouse(int button, int state, int x, int y)
{
	//	Desna tipka - brise canvas.
	if (button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN)
	{
		ociste.x=0;
		updatePerspective();
		glutPostRedisplay();
	}
}

//*********************************************************************************
//	Tastatura tipke - esc - izlazi iz programa.
//*********************************************************************************

void myKeyboard(unsigned char theKey, int mouseX, int mouseY)
{
	switch (theKey)
	{
        case 'c':
            mode = true;
            polygonCnt = 0;
            vertices.clear();
            vertices2.clear();
            vertices3.clear();
            polygons.clear();
            normale.clear();
            normaleIzr.clear();
            printf("Zadajte datoteku objekta: ");
            cin >> fileName;
            object.open(fileName);
            while ( getline (object,line) ) {
                if (line[0] == 'v') {
                    size_t pos = 0;
                    string token;
                    string delimiter = " ";
                    vector<float> v;
                    while ((pos = line.find(delimiter)) != string::npos) {
                        token = line.substr(0, pos);
                        if (token.compare("v") == 0) {
                            line.erase(0, pos + delimiter.length());
                            continue;
                        }
                        v.push_back(stof(token));
                        line.erase(0, pos + delimiter.length());
                    }
                    vertices.push_back(make_tuple(v[0], v[1], stof(line)));
                    vertexCnt++;
                }
                if (line[0] == 'f') {
                    size_t pos = 0;
                    string token;
                    string delimiter = " ";
                    vector<int> v;
                    while ((pos = line.find(delimiter)) != string::npos) {
                        token = line.substr(0, pos);
                        if (token.compare("f") == 0) {
                            line.erase(0, pos + delimiter.length());
                            continue;
                        }
                        v.push_back(stoi(token));
                        line.erase(0, pos + delimiter.length());
                    }
                    normale[v[0]].push_back(polygonCnt);
                    normale[v[1]].push_back(polygonCnt);
                    normale[stoi(line)].push_back(polygonCnt);
                    polygons.push_back(make_tuple(v[0], v[1], stoi(line)));
                    polygonCnt++;
                }
            }
            object.close();

            xmax = get<0>(vertices[0]);
            xmin = get<0>(vertices[0]);
            ymax = get<1>(vertices[0]);
            ymin = get<1>(vertices[0]);
            zmax = get<2>(vertices[0]);
            zmin = get<2>(vertices[0]);
            for ( const auto& i : vertices ) {
                //cout << get<0>(i) << get<1>(i) << get<2>(i) << endl;
                if (get<0>(i) > xmax) xmax = get<0>(i);
                if (get<0>(i) < xmin) xmin = get<0>(i);
                if (get<1>(i) > ymax) ymax = get<1>(i);
                if (get<1>(i) < ymin) ymin = get<1>(i);
                if (get<2>(i) > zmax) zmax = get<2>(i);
                if (get<2>(i) < zmin) zmin = get<2>(i);
            }
            //cout << xmax << xmin << ymax << ymin << zmax << zmin << endl;
            middlex = (xmax + xmin) / 2.0;
            middley = (ymax + ymin) / 2.0;
            middlez = (zmax + zmin) / 2.0;
            if (xmax > ymax) maxAll = xmax;
            else maxAll = ymax;
            if (zmax > maxAll) maxAll = zmax;
            //cout << middlex << middley << middlez << "\n";
            for ( const auto& i : vertices ) {
                //cout << get<0>(i) << get<1>(i) << get<2>(i) << endl;
                vertices2.push_back(make_tuple(get<0>(i) - middlex, get<1>(i) - middley, get<2>(i) - middlez));
            }
            xmax = get<0>(vertices2[0]);
            xmin = get<0>(vertices2[0]);
            ymax = get<1>(vertices2[0]);
            ymin = get<1>(vertices2[0]);
            zmax = get<2>(vertices2[0]);
            zmin = get<2>(vertices2[0]);
            for ( const auto& i : vertices2 ) {
                //cout << get<0>(i) << get<1>(i) << get<2>(i) << endl;
                if (get<0>(i) > xmax) xmax = get<0>(i);
                if (get<0>(i) < xmin) xmin = get<0>(i);
                if (get<1>(i) > ymax) ymax = get<1>(i);
                if (get<1>(i) < ymin) ymin = get<1>(i);
                if (get<2>(i) > zmax) zmax = get<2>(i);
                if (get<2>(i) < zmin) zmin = get<2>(i);
            }
            //cout << xmax << xmin << ymax << ymin << zmax << zmin << endl;
            middlex = (xmax + xmin) / 2.0;
            middley = (ymax + ymin) / 2.0;
            middlez = (zmax + zmin) / 2.0;
            //cout << middlex << middley << middlez << "\n";
            if (xmax > ymax) maxAll = xmax;
            else maxAll = ymax;
            if (zmax > maxAll) maxAll = zmax;
            for ( const auto& i : vertices2 ) {
                //cout << get<0>(i)/maxAll << get<1>(i)/maxAll << get<2>(i)/maxAll << endl;
                vertices3.push_back(make_tuple(get<0>(i)/maxAll, get<1>(i)/maxAll, get<2>(i)/maxAll));
            }
            for(auto& j : normale) {
                float x = 0;
                float y = 0;
                float z = 0;
                float cnt = 0;

                for( auto& index : j.second ) {
                    auto i = polygons[index];

                    float A = (get<1>(vertices3[get<1>(i) - 1]) - get<1>(vertices3[get<0>(i) - 1]))*(get<2>(vertices3[get<2>(i) - 1]) - get<2>(vertices3[get<0>(i) - 1]))
                    - (get<2>(vertices3[get<1>(i) - 1]) - get<2>(vertices3[get<0>(i) - 1]))*(get<1>(vertices3[get<2>(i) - 1]) - get<1>(vertices3[get<0>(i) - 1]));

                    float B = - (get<0>(vertices3[get<1>(i) - 1]) - get<0>(vertices3[get<0>(i) - 1]))*(get<2>(vertices3[get<2>(i) - 1]) - get<2>(vertices3[get<0>(i) - 1]))
                    + (get<2>(vertices3[get<1>(i) - 1]) - get<2>(vertices3[get<0>(i) - 1]))*(get<0>(vertices3[get<2>(i) - 1]) - get<0>(vertices3[get<0>(i) - 1]));

                    float C = (get<0>(vertices3[get<1>(i) - 1]) - get<0>(vertices3[get<0>(i) - 1]))*(get<1>(vertices3[get<2>(i) - 1]) - get<1>(vertices3[get<0>(i) - 1]))
                    - (get<1>(vertices3[get<1>(i) - 1]) - get<1>(vertices3[get<0>(i) - 1]))*(get<0>(vertices3[get<2>(i) - 1]) - get<0>(vertices3[get<0>(i) - 1]));

                    x += A;
                    y += B;
                    z += C;
                    cnt ++;
                }
                x /= cnt;
                y /= cnt;
                z /= cnt;

                normaleIzr.push_back(make_tuple(x, y, z));
            }

            break;

        case 'i':
            printf("Upisite koordinate izvora: ");
            cin >> izvor.x;
            cin >> izvor.y;
            cin >> izvor.z;
            break;

		case 'l':
            ociste.x = ociste.x+0.1;
            izvor.x += 0.1;
        break;

		case 'k':
            ociste.x =ociste.x-0.1;
            izvor.x -= 0.1;
        break;

		case 'r': ociste.x=0.0;
        break;

        case 'g': mode = false;
        break;

        case 'G': mode = true;
        break;

		case 27:  exit(0);
		break;
	}
	updatePerspective();
	glutPostRedisplay();
}
